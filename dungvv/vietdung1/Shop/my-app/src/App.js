import './App.css';
import Home from './component/home/Home';
import Menu from './component/menu/Menu';
import { Routes, Route } from "react-router-dom";
import Product from './component/products/Product';
import Products from './component/products/Products';


function App() {
  return (
    <div className="App">
      <Menu />
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/product" element={<Product />} />
        <Route exact path="/products:id" element={<Products />} />
      </Routes>

    </div>
  );
}

export default App;
