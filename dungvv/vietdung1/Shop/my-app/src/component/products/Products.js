import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'

export default function Products() {

    const { id } = useParams();
    const [product, setProduct] = useState([]);

    useEffect(() => {
        const getProduct = async () => {
            const response = await fetch(`https://fakestoreapi.com/products/${id}`);
            setProduct(await response.json());
        }
        getProduct();
    }, []);


    const ShowProduct = () => {
        return (
            <>
                <div className='col-md-6'>
                    <img src={product.image} alt={product.title} height="400px" width="400px" />
                </div>
                <div className='col-md-6'>
                    <h4 className='text-uppercase text-black-50'>
                        {product.category}
                    </h4>
                    <h1 className='display-5'>{product.title}</h1>
                </div>
            </>
        )
    }

    return (

        <div className='container'>
            <div className='row'>
                <ShowProduct />
            </div>
        </div>

    )
}
