import React from 'react'
import './menu.css'
import logo from '../../img/logo.png'
import { NavLink } from 'react-router-dom'

export default function Menu() {
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container">
                    <NavLink className="navbar-brand" to="/">
                        <img src={logo} />
                    </NavLink>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon" />
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mx-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/">Home</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/product">Products</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/about">About</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/contact">Contact</NavLink>
                            </li>      
                        </ul>
                        <div className="buttons">
                            <NavLink to='/login' className='btn btn-outline-dark me-2'><i class="fa-solid fa-arrow-right-to-bracket"></i> Log In</NavLink>
                            <NavLink to='/register' className='btn btn-outline-dark me-2'><i class="fa-solid fa-arrow-right-to-bracket"></i> Register</NavLink>
                            <NavLink to='/Cart' className='btn btn-outline-dark me-2'><i class="fa-solid fa-cart-shopping"></i> Cart (0)</NavLink>
                        </div>
                    </div>
                </div>
            </nav>

        </div>
    )
}
