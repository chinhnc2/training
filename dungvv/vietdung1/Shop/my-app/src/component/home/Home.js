import React from 'react'
import './home.css'
import bg from '../../img/bg.jpg'
import Product from '../products/Product'

export default function Home() {
    return (

        <div className='hero'>
            <div className="card bg-dark text-white border-0">
                <img src={bg} className="card-img" alt="background" />
                <div className="card-img-overlay d-flex flex-column justify-content-center">
                    <div className='container'>
                        <h5 className="card-title fw-bold display-3 mb-0">News Season</h5>
                        <p className="card-text lead fs-2">Check Out The Trend</p>
                    </div>

                </div>
            </div>
            
            <Product />
        </div>

    )
}
