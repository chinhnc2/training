import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./component/Home/Home";
import Login from "./component/Login/Login";
import SignUp from "./component/Signup/Signup";
import { AuthProvider } from "./AuthProvider";
import Nav from "./component/Nav/Nav";
import Calender from './component/calender/Calender'
import Homework from "./component/homework/Homework";

function App() {
  return (
    <AuthProvider>
      <div className="app">
        <BrowserRouter>
          <Nav />
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/signup" element={<SignUp />} />
            <Route exact path="/calender" element={<Calender />} />
            <Route exact path="/homework" element={<Homework />} />
          </Routes>
        </BrowserRouter>
      </div>
    </AuthProvider>
  );
}

export default App;
