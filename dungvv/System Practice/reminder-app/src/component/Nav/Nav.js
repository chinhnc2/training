import React, { useState } from 'react'
import './Nav.css';
import { NavLink } from "react-router-dom";
import logo from '../../img/logo.png'

export default function Nav() {


    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <NavLink className="logo navbar-brand" to="/">
                    <img src={logo} alt="logo" />
                </NavLink>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon" />
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mx-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <NavLink className="btn btn-danger nav-link" to="/calender">Calendar</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="btn btn-danger nav-link" to="/homework">Homework</NavLink>
                        </li>
                    </ul>
                </div>
                <div className='notification m-4'>
                    <button className='btn btn-outline-danger'><i class="fa-solid fa-bell"></i></button>
                </div>
                <div className='buttons'>
                    <NavLink to='/login' className='btn btn-outline-danger me-2'><i class="fa-solid fa-arrow-right-to-bracket"></i> Log In</NavLink>
                    <NavLink to='/signup' className='btn btn-outline-danger me-2'><i class="fa-solid fa-arrow-right-to-bracket"></i> Sign In</NavLink>
                </div>
            </nav>

        </div>
    )
}
