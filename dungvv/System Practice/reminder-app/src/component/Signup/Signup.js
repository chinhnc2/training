import React, { useState } from "react";
import { auth, db } from "../Base/Base";
import "./Signup.css";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { useNavigate } from "react-router-dom";
import { ref, set } from "firebase/database";
import logo from '../../img/logo.png'

const SignUp = () => {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();

    const handleSubmit = (e) => {
        e.preventDefault();
        function onRegister() {
            createUserWithEmailAndPassword(auth, email, password)
                .then((userCredential) => {
                    console.log('k');
                    set(ref(db, "users/" + userCredential.user.uid), {
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        password: password
                    });
                })
                .catch((error) => console.log(error));
            navigate("/");
        }
        onRegister();
    };

    return (
        <div>

            <div className="container">
            <img src={logo} alt="" />
                <div className="row">
                    <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                        <div className="card border-0 shadow rounded-3 my-5">
                            <div className="card-body p-4 p-sm-5">
                                <h5 className="card-title text-center mb-5 fw-light fs-5">Sign Up</h5>

                                <form className="signupForm" onSubmit={handleSubmit}>
                                    <div className="form-floating mb-3">
                                        <input type="text" onChange={(e) => setFirstName(e.target.value)} required className="form-control" placeholder="First Name" />
                                        <label htmlFor="floatingInput">First Name</label>
                                    </div>
                                    <div className="form-floating mb-3">
                                        <input type="text" onChange={(e) => setLastName(e.target.value)} required className="form-control" placeholder="Last Name" />
                                        <label htmlFor="floatingPassword">Last Name</label>
                                    </div>
                                    <div className="form-floating mb-3">
                                        <input type="email" onChange={(e) => setEmail(e.target.value)} required className="form-control" placeholder="Email" />
                                        <label htmlFor="floatingPassword">Email</label>
                                    </div>
                                    <div className="form-floating mb-3">
                                        <input type="password" onChange={(e) => setPassword(e.target.value)} required className="form-control" placeholder="Password" />
                                        <label htmlFor="floatingPassword">Password</label>
                                    </div>

                                    <div className="d-grid">
                                        <button className="btn btn-primary btn-login text-uppercase fw-bold" type="submit">Sign Up</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>







            {/* <form className="signupForm" onSubmit={handleSubmit}>
                <input placeholder="first name" onChange={(e) => setFirstName(e.target.value)} required ></input>
                <input placeholder="last name" onChange={(e) => setLastName(e.target.value)} required></input>
                <input placeholder="email" onChange={(e) => setEmail(e.target.value)} required type="email"></input>
                <input placeholder="password" onChange={(e) => setPassword(e.target.value)} required type="password"></input>
                <button>Sign Up</button>
            </form> */}
        </div>
    );
};

export default SignUp;