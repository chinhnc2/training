import React from 'react'

// import firebase from '../Base/Base'
import { getFirestore, collection, getDocs } from 'firebase/firestore'

export default function Homework() {
    const db = getFirestore()

    const colRef = collection(db, 'homework');
    getDocs(colRef)
        .then((snapshot) => {
            let list = []
            snapshot.docs.forEach((doc) => {
                list.push({ ...doc.data(), id: doc.id })
            })
            console.log(list);
        })
        .catch(err => {
            console.log(err.message);
        })

    return (
        <div>
            <h2 className='container' style={{ fontWeight: 'bold', textAlign: 'center', marginTop: '50px', marginBottom: '50px' }}>Home Work</h2>
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Class</th>
                        <th scope="col">Location</th>
                        <th scope="col">Lectures</th>
                        <th scope="col">HomeWork</th>
                    </tr>
                </thead>
                <tbody>
                    {/* {
                        db.map(data => {
                            return (
                                <tr>
                                    <th scope="row">1</th>
                                    <td>{data.Name}</td>
                                    <td>{data.Class}</td>
                                    <td>{data.Location}</td>
                                    <td>{data.Lectures}</td>
                                    <td>{data.Homework}</td>
                                </tr>
                            )
                        })
                    } */}
                    <tr>
                        <th scope="row">1</th>
                        <td>Name</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Larry the Bird</td>
                        <td>@twitter</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>@mdo</td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}
