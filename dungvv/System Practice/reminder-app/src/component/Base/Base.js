import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";
import { getDatabase } from "firebase/database";


const firebaseConfig = {
  apiKey: "AIzaSyDBMT7VbZuBH-CsJa08w7Wq0WxdANIu0i0",
  authDomain: "reminder-app-ced26.firebaseapp.com",
  databaseURL: "https://reminder-app-ced26-default-rtdb.firebaseio.com",
  projectId: "reminder-app-ced26",
  storageBucket: "reminder-app-ced26.appspot.com",
  messagingSenderId: "33333331907",
  appId: "1:33333331907:web:86e0f7b25b59bcd1351f76",
  measurementId: "G-3N1JRYPCSM"
};


const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);


export const auth = getAuth();
export const db = getDatabase(app);
export default app;