var jsons = [
    {
        time: 5,
        checkIn: 1645525689097,
        checkOut: 1645525742336
    },
    {
        time: 10,
        checkIn: 1645525706832,
        checkOut: 1645525758920
    },
    {
        time: 14,
        checkIn: 1645525724569,
        checkOut: 1645525774743
    },
    {
        time: 20,
        checkIn: 1645424597980,
        checkOut: 1645425143574
    },
    {
        time: 27,
        checkIn: 1645425585843,
        checkOut: 1645425586979
    }
];

function renderCheckInfos(months, jsons) {

    var days = [];
    var date = new Date(2022, months - 1, 1);


    while (date.getMonth() === months - 1) {
        var objTime = {};
        var day = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
        objTime.day = day;
        var timeTrue = jsons.find((json) => {
            return date.getDate() === json.time;
        })//ở đây tạo 1 bên timeTrue, dùng hàm find của mảng sẽ tìm ra 1 giá trị mà thỏa mãn 1 điều kiện
        // khi time của 1 phần tử trong mảng đúng với date trong dữ liệu thì nó sẽ lưu vào biến timeTrue
        // nếu tìm được thì sẽ lưu còn ko thì undefined

        if (timeTrue !== undefined) {
            objTime.checkIn = timeTrue.checkIn;
            objTime.checkOut = timeTrue.checkOut;
        }
        // check biến timeTrue nếu mà không bằng undefined thì tức là ở đây có,
        //thì nó sẽ lưu những biến checkIn checkOut vào cái objTime, xong đẩy vào mảng
        else {
            objTime.checkIn = null;
            objTime.checkOut = null;
        }// ngược lại nếu không có thì checkIn checkOut = null
        days.push(objTime);
        date.setDate(date.getDate() + 1);
    }
    return days;



    //................................................................

    // return về một mảng có số phần tử là số ngày trong tháng từ 1 tới
    // Trong mỗi phần tử có dữ liệu checkin và checkout
}
console.log(renderCheckInfos(2, jsons))
