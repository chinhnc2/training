// 'use strict'
console.log("võ viết dũng");
// 1) Declarations (var, let, const) Biến là vùng chứa để lưu trữ dữ liệu (lưu trữ các giá trị dữ liệu).

// var x = 5;
// let times = 4;
// const PI = 3.14159265359;

// 2) Variable scope có 3 loại Block scope, Function scope, Global scope

// {
//     let x = 2;
// } // block scope

// function myFunction() {
//     var carName = "Volvo";
// }// Function Scope

//var x = 2; //Global scope

// 3) Variable hoisting ----- có thể sử dụng trước khi khai báo 
// example of variable hoisting
//x = 5;

// var sum = x + 6;

// var x;
// console.log(sum);

// 4) Function hoisting -----
// example of function hoisting
// catName("Tiger");

// function catName(name) {
//     console.log("My cat's name is " + name);
// }

// 5) Global variables ----- 
//var x = 2;// một Global variables

// 6) Data types ----- kiểu dữ liệu : number, string, obj, booleans ...
// let length = 16;
// let firstName = "vietDung";



// 7) Converting strings to numbers (chuyển đổi ký tự thành số) ,Type coercion (hiểu theo đơn giản là chuyển đổi số thành chuỗi, chuỗi thành sô, boolean thành số ,....)
// number("14.08") => 14.08

// 8) Literals for Array, Object, Numeric, RegExp, String
// + array : const cars = ["Saab", "Volvo", "BMW"];
// + obj là những thuộc tính ngoài đời thực (ví dụ: name, weight, height ,...)
// + string chuỗi 

// 9) Block statement { }
// console.log("Ex Block statement");
// var age;
// if (age < 10) {
//     console.log("young buffalo");
// } else {
//     console.log("young");
// }


// 10) Exception handling statements (try...catch) -----
// Câu lệnh try cho phép bạn xác định một khối mã để kiểm tra lỗi trong khi nó đang được thực thi.
// Câu lệnh catch cho phép bạn xác định một khối mã sẽ được thực thi nếu có lỗi xảy ra trong khối try.
// console.log("EX try catch");
// try {
//     // Quăng lỗi ra
//     console.log(message);
// } catch (e) {
//     // Đón nhận lỗi và in ra
//     // Vị trí này chỉ chạy khi ở try có quăng lỗi hoặc ở try 
//     // sử dụng sai cú pháp ...
//     console.log(e.message);
// }
// 11) Operators, spread operator ----- (được viết là ...) thêm phần tử vào mảng, kết hợp mảng, truyền tham số và function ...

// 12) Loops and iteration ----- Loop là vòng lặp for, iteration hoạt động trên mọi mảng

// 13) Defining functions, IIFE

// 14) Function scope ----- Mỗi chức năng tạo ra một phạm vi mới
// console.log("Function scope");
// function scope() {
//     var a = Math.random();
//     console.log(a);
// }
// scope();

// 15) Scope and the function stack ----- scope nghĩa là phạm vi: global(khai báo xong thì có thể dùng ở tất cả mọi nơi trong chương trình) toàn cầu,
//  code block khối mãi (var, let), local scope ham (var, function)

// 16) closures là một hàm có thể ghi nhớ nơi nó được tạo và truy  cập được biến ở ngoài phạm vi của nó
// console.log("Ex closures");

// function createCouter() {
//     var a = 0;

//     function couter1() { //closures
//         return ++a;
//     }

//     return couter1;

// }
// var number = createCouter()

// console.log(number());
// console.log(number());
// console.log(number());

// 17)Arguments object ----- biến tồn tại bên trong 1 cái hàm
// console.log("Ex Arguments object");
// const obj = {
//     0: 'dung',
//     1: 'hung',
//     2: 'mai',
//     length: 3
// }
// function sum(){
//     console.log(argument);
// }
// sum(1, 23, 45);
// for (var i = 0; i < obj.length; i++) {
//     console.log(obj[i]);
// }
// 18) Function parameters (default, rest) ----- giá trị tham số mặc định
// function logger(log = 'gia tri mac dinh') {
//     console.log(log);
// }
// logger("abc"); // truyền vào cái gì in ra cái đó, không truyền thì lấy giá trị mặc định

// 19) Arrow functions
// console.log("Ex Arrow Func");
// const ArrowFunc = (a, b) => a + b;

// console.log(ArrowFunc(2, 4));


// 21) Conditional (ternary) operator ----- toán tử 3 ngôi
// console.log("Ex Conditional (ternary) operator");
// var a = 1;
// var b = 2;
// var result = a > 0 ? a : b;
// console.log(result);

// 22) String operators,Comma operator ---------------------------- chú ý xem lại err
// function x1() {
//     console.log("one");
//     return 'one';
// }
// function y() {
//     console.log("two");
//     return 'two';
// }
// function z() {
//     console.log("three");
//     return 'three';
// }

// var kq = (x1(), y(), z());
// console.log(kq);

// 23) Unary operators (delete, typeof, void)
// console.log("Ex Unary operators delete");
// var variableExample = 1;
// delete variableExample;          // returns false
// console.log(variableExample);    // returns 1

// console.log("Ex Unary operators typeof");
// console.log(typeof 'vietdung');
// console.log(typeof 4);

// console.log("Ex Unary operators void"); // chưa hiểu lắm
// var functionExample = function () {
//     console.log('Example')
//     return 4;
// }
// var result = functionExample()
// console.log(result);
// var voidResult = void (functionExample())
// console.log(voidResult);


// 24) Primary expressions - this ----------literal values, variable references, and some keywords.
// this['propertyName']
// this.propertyName
// this.name
// this.displayName()

// 25) Left-hand-side expressions (new, supper)
// var objectName = new objectType([param1, param2, ... paramN]);
//You can use the new operator to create an instance of a user-defined object type or of one of the built-in object types.

//super([arguments]); // calls the parent constructor.
// super.functionOnParent([arguments]);
//Từ khóa super được sử dụng để gọi các hàm trên cha của một đối tượng.

// 26) Regular expressions (RegExp)


// 27) JSON --- định dạng dữ liệu ( chuỗi )
//  JSON thể hiện được kiểu dữ liệu dạng number, string, boolean, Array, obj ...
// mã hóa (Encode)--- giải mã (Decode)
// stringify ------ từ js types -> json 
// Parse ------ từ json -> js types

//var a = '1';// kiểu dữ liệu number
//var b = 'true'; // kiểu dữ liệu boolean

//var json = '["js", "java"]'; // kiểu dữ liệu Arr
//console.log(json);

// var json = '{"name":"Viet Dung", "age": 24}'; // object
// console.log(json);

// var a ="1";
// console.log(JSON.parse(a));


// 28) Object and Classes
// Object
// var myObj = {
//     //key: value
//     name: 'dung',
//     age: 24,
//     address: 'QT',
//     getName: function(){
//         return this.name;
//     } // create mot function
// };
// myObj.email = 'vietdung1482000@gmail.com'
// var myKey = 'age';

// delete myObj.age;
// console.log(myObj.getName());
// function ===> Phuong thuc/ method
// others ===> thuoc tinh/ property

// classes có constructor định nghĩa phương thức, thuộc tính
// function food(name, price){
//     this.name = name;
//     this.price = price;
//     //thuoc tinh

//     var getName = function (){
//         return this.name;
//     }
//     // phuong thuc
//     const isSuccess = true;
//     //bien
// }

// class food{
//     constructor(name, price){
//         this.name = name;
//         this.price = price;
//     }
//     getName(){
//         return this.name;
//     }
// }
// const meat = new food('thit', 1000);
// const fish = new food('ca', 1200);

// console.log(meat);
// console.log(fish);


// 29) Strict mode
// "use strict" đưa lên đầu file js, nghĩa là code nghiêm ngặt


// 30) Modules:import/export
// import logger from './Module.js';

// logger('messega ....', 'error')
// console.log(logger);


// 31) Callbacks là hàm / được truyền qua đối só / được gọi lại

// 34) Async/Await
// Async - khai báo một hàm bất đồng bộ (async function someName(){...}).
// Await - tạm dừng việc thực hiện các hàm async. (Var result = await someAsyncCall ()

// 35) Generator function ///// Generator là obj được trả về từ Generator function
// function* GeneratorId() {
//     var i = 0;
//     while (true) {
//         i++;
//         yield 1;
//     }
// }
// const newId = GeneratorId();
// newId.next();
// newId.next();
// newId.next();

// 36) HTML DOM: là dạng cây, mô hình của trang web

// 37) BOM - Window
// window.document.getElementById("header"); == document.getElementById("header");
// 38)BOM - Location
// console.log("The full URL of this page is:" + window.location.href);
// 39)BOM - History
// function goBack() {
//     window.history.back()
// }
// goBack();
// 40)BOM - Alert
// 41)BOM - Cookies
// document.cookie = "username=viet dung";

// document.cookie = "username=John Smith";
// document.cookie = "username=";

// let x = document.cookie;
// console.log(x);

// 42)BOM - Screen
// console.log("Screen Width: " + screen.width);

// 43) Web APIs