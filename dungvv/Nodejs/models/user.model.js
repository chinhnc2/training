var mongoose = require('mongoose')

var useSchema = new mongoose.Schema({
    email: String,
    name: String,
    address: String,
    phone: String
});

var User = mongoose.model('user', useSchema, "Customer")
module.exports = User;