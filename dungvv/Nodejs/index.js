var express = require('express') // tra ve app
var app = express() // goi app 
var port = 5000 // server
var User = require('./models/user.model');
var connectDB = require('./config/db')
var db = require('./controller/user.controller')

// lay ten nguoi dung
app.get('/user', async (req, res) => {
    var users = await User.find();
    res.status(200).json(users)
})

app.listen(port, function () {
    console.log("App listening on port " + port)
});