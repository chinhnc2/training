import React, { useState } from 'react'
import './App.css';
import { useCart } from "react-use-cart";

// { item, fn }
export default function ItemCart(props) {   
    const [listStore, setListStore] = useState([]);
    const { addItem } = useCart();

    const addToCart = (item) => {
        addItem(item);
        if (listStore.indexOf(item.nameStory) < 0) {
            let temp = [...listStore, item.nameStory];
            setListStore(temp);
        }
    }
    return (
        
        <div className='col-11 col-sm-2 mx-0 mb-4'>
            <div className="card p-0 overflow-hidden h-100 shadow">
                <img className="img card-img-top img-fluid" src={props.img} />
                <div className="card-body">
                    <h5 className="card-title">{props.title}</h5>
                    <h5 className="card-text">{props.price} $</h5>
                    <p className="card-text">{props.desc}</p>
                    <button onClick={() => addToCart(props.item)} className="btn btn-primary">Buy</button>
                </div>
            </div>
        </div>
    )
}
