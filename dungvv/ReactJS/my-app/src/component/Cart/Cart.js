import React from 'react'
import './App.css'
import { useCart } from "react-use-cart";


export default function Cart() {
  const {
    isEmpty,
    items,
    totalUniqueItems,
    totalItems,

    cartTotal,
    updateItemQuantity,
    removeItem,
    emptyCart,
  } = useCart();

  if (isEmpty) return <h1 className='text-center'>Your cart is Empty</h1>
  return (

    <section className='py-4 container'>
      <div className='row justify-content-center'>
        <div className='col-12'>
          <h4>Cart ({totalUniqueItems}) total Item: ({totalItems})</h4>
          <table className='table table-light table-hover m-0'>
            {/* bọc 1 cái div với LISTSTORE.map(x ) */}
            <tbody>
              {
                items.map((item, index) => {
                  // item.nameS === x
                  return (
                    <tr key={index}>
                      <td>
                        <h4>{item.nameStory}</h4>
                      </td>
                      <td>
                        <img src={item.img}  alt=""/>
                      </td>
                      <td>{item.title}</td>
                      <td>{item.price}</td>
                      <td>Quantity: ({item.quantity})</td>
                      <td>
                        <button className='btn btn-info' onClick={() => updateItemQuantity(item.id, item.quantity + 1)}>+</button>
                        <button className='btn btn-success' onClick={() => updateItemQuantity(item.id, item.quantity - 1)}>-</button>
                        <button className='btn btn-danger' onClick={() => removeItem(item.id)} >Remove Item</button>
                      </td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
        </div>
        <div className='price'>
          <h2>Cart Total: {cartTotal} $</h2>
        </div>
        <div className='cleanCart'>
          <button onClick={() => emptyCart()} className='btn btn-danger'>Clean Cart</button>
          <button className='btn btn-info'>Buy</button>
        </div>
      </div>
    </section>
  )
}
