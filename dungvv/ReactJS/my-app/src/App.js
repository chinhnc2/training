import './App.css';
import React, { useState } from 'react';
import { CartProvider } from "react-use-cart";

import Cart from './component/Cart/Cart';
import json from './Json.json';
import ItemCart from './component/ItemCart/ItemCart';


function App() {

  const [searchTerm, setSeaarchTerm] = useState('')
  return (
    <CartProvider>
      <div className="App">

        {/* Menu */}
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <a className="nameShop navbar-brand" href="#">Alexamder*King</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <a className="nav-link" href="#">Home</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">Contact</a>
              </li>
            </ul>
            <form className="form-inline my-2 my-lg-0">
              <input className="form-control mr-sm-2" onChange={event => { setSeaarchTerm(event.target.value) }} type="search" placeholder="Search" aria-label="Search" />
              <button className="btn btn-info my-2 my-sm-0" type="submit">Search</button>
            </form>

            <div className='icons'>
              <a href='#'> <i class="iconUser fa-solid fa-user"></i></a>
              <a href='#'><i class="iconCart fa-solid fa-cart-shopping"></i></a>
            </div>
          </div>
        </nav>

        {/* home */}
        <div className='home'>
          <h1 className='text-center'>All Product</h1>
          <section className='py-4 container'>
            <div className='row justify-content-center'>
              {
                json.filter((value) => {
                  if (searchTerm === "") {
                    return value;
                  } else if (value.title.toLowerCase().includes(searchTerm.toLowerCase())) {
                    return value;
                  }
                }).map((item, index) => {
                  return (
                    <ItemCart img={item.img} title={item.title} desc={item.desc} price={item.price} key={index} item={item} />
                  )
                })
              }
            </div>
          </section>
        </div>

{/* đưa cái hàm addToCart, addItem qua bên Appjs, đưa thêm cái state listStore, truyền hàm addtoCart vô ItemCart. Truyền storeId vô Cart */}

        <Cart />

      </div>
    </CartProvider>
  );
}
export default App;