import mongoose from "mongoose";

const schema = new mongoose.Schema(
  {
    name: {
      type: String,
      require: true,
    },
    price: {
      type: Number,
      require: true,
    },
    image: {
      type: String,
      require: true,
    },
    store: {
      type: String,
      require: true,
    },
    storeID: {
      type: String,
      require: true,
    },
  },
  { timestamps: true }
);

export const productModel = mongoose.model("products", schema);
