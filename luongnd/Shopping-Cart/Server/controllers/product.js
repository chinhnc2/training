import { productModel } from "../models/productModel.js";

export const getProduct = async (req, res) => {
    try {
        const product = await productModel.find();
        console.log('products', product);
        res.status(200).json(product);
    } catch(err) {
        res.status(500).json({error: err});
    }
};

export const createProduct = async (req, res) => {
    try {
        const newProduct = req.body;

        const product = new productModel(newProduct);
        await product.save();

        res.status(200).json(product);
    } catch(err) {
        res.status(500).json({error: err});
    }
};

export const updateProduct = async (req, res) => {
    try {
        const updateProduct = req.body;

        const product = await productModel.findOneAndUpdate({ _id: updateProduct._id }, updateProduct, { new: true });

        res.status(200).json(product);
    } catch(err) {
        res.status(500).json({error: err});
    }
};