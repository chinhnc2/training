import React from "react";

export default function Header(props) {
  return (
    <header className="block center">
      <img
        src="https://www.udemy.com/staticx/udemy/images/v7/logo-udemy.svg"
        alt="logo udemy"
        height="40px"
        width="90px"
      />

      <div className="header-cart">
        <a href="#/cart">
          Cart{" "}
          {props.countCartItems ? (
            <button className="badge">{props.countCartItems}</button>
          ) : (
            ""
          )}
        </a>{" "}
        <a href="#/signin">Log in</a>
      </div>
    </header>
  );
}
