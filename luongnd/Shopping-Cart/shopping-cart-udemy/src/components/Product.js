import React from "react";

export default function Product(props) {
  const { product, onAdd } = props;

  return (
    <div className="item-product border1">
      <div className="detail-product">
        <img className="small" src={product.image} alt={product.name} />
        <h3>{product.name}</h3>
        <div className="price">${product.price}</div>
      </div>
      <div className="show">
        <button className="btn-add-cart" onClick={() => onAdd(product)}>
          Add To Cart
        </button>
      </div>
    </div>
  );
}
