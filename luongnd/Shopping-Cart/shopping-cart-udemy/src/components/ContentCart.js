import React from "react";

export default function ContentCart(props) {
  const { cartItems, storeIDs, onAdd, onRemove } = props;

  return (
    <div className="col-2 ">
      {
        // cartItems.length >= 0 &&
        storeIDs.map((item1) => (
          <div
          //  key={item1.id}
          >
            {/* show store */}
            <div>
              <div className="store">
                <img
                  src="https://salt.tikicdn.com/ts/upload/30/24/79/8317b36e87e7c0920e33de0ab5c21b62.png"
                  alt="group product"
                />
                <a href="#/" className="store">
                  {item1}
                  <img
                    src="https://frontend.tikicdn.com/_desktop-next/static/img/icons/Path.svg"
                    alt="arrow"
                  />
                </a>
              </div>
            </div>
            <div className="list-item-cart">
              {
                // cartItems.length > 0 &&
                cartItems.map(
                  (item) =>
                    item1 === item.storeID && (
                      <div className="item-cart" key={item._id}>
                        {/* show item product */}
                        <div className="row">
                          <img
                            className="small"
                            src={item.image}
                            alt={item.name}
                          />
                          <div className="col-2">{item.name}</div>
                          <div className="col-2">
                            <button
                              onClick={() => onRemove(item)}
                              className="remove"
                            >
                              -
                            </button>{" "}
                            <button onClick={() => onAdd(item)} className="add">
                              +
                            </button>
                          </div>
                          <div className="col-2 text-right">
                            {item.qty} x ${item.price.toFixed(2)}
                          </div>
                        </div>
                      </div>
                    )
                )
              }
            </div>
          </div>
        ))
      }
    </div>
  );
}
