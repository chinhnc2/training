import React from "react";
import Product from "./Product";

import { useDispatch, useSelector } from "react-redux";
import * as actions from "../redux/actions";

import { productState$ } from "../redux/selectors";

export default function Main(props) {
  const dispatch = useDispatch();
  const products = useSelector(productState$);
  console.log(`product main: `, products);

  React.useEffect(() => {
    dispatch(actions.getproduct.getProductRequest());
  }, [dispatch]);

  const {
    // products1,
    onAdd,
  } = props;
  return (
    <main className="block block-col col-2">
      <h2>Products</h2>
      <div className=" list-products">
        {products.map((product) => (
          <Product key={product._id} product={product} onAdd={onAdd}></Product>
        ))}
      </div>
    </main>
  );
}
