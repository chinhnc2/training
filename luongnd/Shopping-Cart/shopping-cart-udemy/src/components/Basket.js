import React from "react";

export default function Basket(props) {
  const {
    cartItems,
    // , onAdd, onRemove, product
  } = props;
  const itemsPrice = cartItems.reduce((a, c) => a + c.qty * c.price, 0);
  const totalPrice = itemsPrice; //tổng giá

  return (
    <aside className="block col-1 block-table">
      <div>
        {cartItems.length === 0 && (
          <div>Your cart is empty. Keep shopping to find a product!</div>
        )}
        {cartItems.length !== 0 && (
          <>
            <hr></hr>
            <div className="row">
              <div className="col-2">
                <strong>Total Price</strong>
              </div>
              <div className="col-1 text-right">
                <strong>${totalPrice.toFixed(2)}</strong>
              </div>
            </div>
            <hr />
            <div>
              <button
                className="checkout"
                onClick={() => alert("Implement Checkout!")}
              >
                Checkout
              </button>
              <h4 className="margintb">Promotions</h4>
              <form className="promotions">
                <input
                  className="input-promotions"
                  type="text"
                  placeholder=" Enter Coupon"
                />
                <button
                  className="btn-promotions"
                  onClick={() => alert("Implement Checkout!")}
                >
                  Apply
                </button>
              </form>
            </div>
          </>
        )}
      </div>
    </aside>
  );
}
