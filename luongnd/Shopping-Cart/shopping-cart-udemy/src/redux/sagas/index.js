import { call, put, takeLatest } from "redux-saga/effects";
import * as actions from "../actions";
import * as api from "../../api";

function* fetchProductSaga(action) {
  const product = yield call(api.fetchProduct);
  // console.log('[product]', product);
  yield put(actions.getproduct.getProductSuccess(product.data));
}

function* mySaga() {
  yield takeLatest(actions.getproduct.getProductRequest, fetchProductSaga);
}

export default mySaga;
