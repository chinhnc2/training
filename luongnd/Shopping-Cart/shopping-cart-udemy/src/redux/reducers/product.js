import { INIT_STATE } from "../../constant";
import { getproduct, getType } from "../actions";

export default function productReducers(state = INIT_STATE.product, action) {
  switch (action.type) {
    case getType(getproduct.getProductRequest): //case getproductRequest
      return {
        ...state,
        isLoading: true,
      };
    case getType(getproduct.getProductSuccess): //case getproductSuccess
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    case getType(getproduct.getProductFailure): //case getproductFailure
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
}
