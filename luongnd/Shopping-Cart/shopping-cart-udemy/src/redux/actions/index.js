import { createActions } from 'redux-actions';

export const getType = (reduxAction) => {
    return  reduxAction().type;
}

export const getproduct = createActions({
    getProductRequest: undefined,
    getProductSuccess: (payload) => payload,
    getProductFailure: (err) => err,
});