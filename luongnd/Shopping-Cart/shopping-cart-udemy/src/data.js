const data = {
  products: [
    {
      id: '1',
      name: 'Apple MacBook M1',
      price: 1400,
      image: 'https://macmall.vn/uploads/pro-m1-13inch-2020-gray_1605757126.png',
      storeID: 'apple',
      store: 'Apple Store'
    },
    {
      id: '2',
      name: 'Apple iMac M1',
      price: 1500,
      image: 'https://www.hnmac.vn/media/cache/data/san-pham/imac/imac-24-inch/blog957-700x530.jpg',
      storeID: 'apple',
      store: 'Apple Store'
    },
    {
      id: '3',
      name: 'Apple Iphone 14',
      price: 1000,
      image: 'https://cdn.tgdd.vn/Files/2021/04/15/1343505/iphone-14-pro-va-14-pro-max-thiet-ke_1280x1280-800-resize.jpg',
      storeID: 'apple',
      store: 'Apple Store'
    },
    {
      id: '4',
      name: 'Apple Watch',
      price: 400,
      image: 'https://didongviet.vn/pub/media/catalog/product/a/p/apple-watch-series-7-41mm-gps-vien-nhom-mau-bac-didongviet.jpg',
      storeID: 'lazada',
      store: 'Lazada'
    },
    {
      id: '5',
      name: 'Polishing Cloth',
      price: 19,
      image: 'https://www.imore.com/sites/imore.com/files/styles/large/public/field/image/2021/10/apple-polishing-cloth.jpg',
      storeID: 'tiki',
      store: 'Tiki'
    },
    {
      id: '6',
      name: 'Apple Airpors Max',
      price: 500,
      image: 'http://maccenter.vn/App_Images/AirPods-Max-Silver.jpg',
      storeID: 'shopee',
      store: 'Shopee'
    },
  ],
};
export default data;
