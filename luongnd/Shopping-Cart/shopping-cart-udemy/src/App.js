import Header from "./components/Header";
import Main from "./components/Main";
import Basket from "./components/Basket";
import ContentCart from "./components/ContentCart";
// import data from "./data";
import { useState } from "react";

import {
  useDispatch,
  // useSelector
} from "react-redux";
import * as actions from "./redux/actions";

function App(props) {
  const { products } = props;
  const dispatch = useDispatch();

  const [cartItems, setCartItems] = useState([]);
  const [storeIDs, setStoreIDs] = useState([]);
  // const dispatch = useDispatch();

  dispatch(actions.getproduct.getProductRequest());

  //add products to cart
  const onAdd = (product) => {
    const exist = cartItems.find((x) => x._id === product._id);
    if (exist) {
      setCartItems(
        cartItems.map((x) =>
          x._id === product._id ? { ...exist, qty: exist.qty + 1 } : x
        )
      );
    } else {
      setCartItems([...cartItems, { ...product, qty: 1 }]);
    }
    addStoreIDs(product.storeID);
  };

  //del product
  const onRemove = (product) => {
    const exist = cartItems.find((x) => x._id === product._id);

    if (exist.qty === 1) {
      setCartItems(cartItems.filter((x) => x._id !== product._id));
      // setCartItems(cartItems.filter((x) => x.storeID !== product.storeID));
      //
    } else {
      setCartItems(
        cartItems.map((x) =>
          x._id === product._id ? { ...exist, qty: exist.qty - 1 } : x
        )
      );
    }
  };

  //add group
  const addStoreIDs = (x) => {
    if (storeIDs.indexOf(x) < 0) {
      setStoreIDs([...storeIDs, x]);
      console.log(storeIDs);
    }
  };

  return (
    <div className="App">
      <Header countCartItems={cartItems.length}></Header>
      <div className="title-cart">Shopping Cart</div>
      <div className="row table">
        <ContentCart
          storeIDs={storeIDs}
          cartItems={cartItems}
          onAdd={onAdd}
          onRemove={onRemove}
        ></ContentCart>
        <Basket cartItems={cartItems}></Basket>
      </div>
      <div className="col">
        <Main products={products} onAdd={onAdd}></Main>
      </div>
    </div>
  );
}

export default App;
