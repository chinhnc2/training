function renderCICO(month, timesheet){
    let year = new Date().getFullYear() // lấy năm hiện tại
    let daysInMonth = new Date(year, month, 0).getDate(); // lấy số ngày trong tháng
    let days = []

    let mm = month < 10 ? `0${month}` : month // thêm 0 vào ngày, tháng để fix

    for (let day = 1; day <= daysInMonth ; day++){

        let dd = day < 10 ? `0${day}` : day
        days.push(new Date(`${year}-${mm}-${dd}`))
    }

    console.log(days) // hiển thị các ngày trong tháng.



    let onDays = 0; // đếm ngày làm việc
    let hoursInDay = []; // số giờ làm trong ngày
    let minutesInDay = []; // số phút làm trong ngày
    let _item;
    let totalHours = 0; // giờ/tháng
    let totalMinutes = 0; // phút/ tháng
    
    days = days.map((day) => {
        let checkCICO = timesheet.find((item) => {
            _item = item;
            return item.time.getTime() == day.getTime() && !!item.checkin && !!item.checkout // điều kiện lọc 
        })
        
        
        if (!!checkCICO) {
            hoursInDay = _item.checkout.getHours() - _item.checkin.getHours() - 1; // số giờ làm trong ngày
            totalHours += hoursInDay; // số giờ làm trong tháng
            minutesInDay = _item.checkout.getMinutes() - _item.checkin.getMinutes() - 15; // số phút làm trong ngày
            totalMinutes += minutesInDay; // số phút làm trong tháng
            onDays++
            return checkCICO
        } else {
            return {
                time: day,
                checkin: null,
                checkout: null
            }
        }    

    })
    // console.log(hoursInDay)
    // console.log(minutesInDay)
    // console.log(totalHours)
    // console.log(totalMinutes)
    console.log(`Business Day/Month: ${onDays}, total number of hours worked: ${totalHours}H:${totalMinutes}'`) // tổng số ngày làm việc trong tháng
    console.log(days); // CICO trong tháng

}




let cicoData = [
    {
        time: new Date('2022-02-01'),
        checkin: new Date('2022-02-01 8:15:30'),
        checkout: new Date('2022-02-01 17:30:30')
    },
    {
        time: new Date('2022-02-06'),
        checkin: new Date('2022-02-06 8:15:30'),
        checkout: new Date('2022-02-06 17:30:30')
    },
    {
        time: new Date('2022-02-14'),
        checkin: new Date('2022-02-14 8:15:30'),
        checkout: new Date('2022-02-14 17:30:30')
    },
    {
        time: new Date('2022-04-01'),
        checkin: new Date('2022-04-01 8:15:30'),
        checkout: new Date('2022-04-01 17:30:30')
    },
    {
        time: new Date('2022-05-01'),
        checkin: new Date('2022-05-01 8:15:30'),
        checkout: new Date('2022-05-01 17:30:30')
    },
    {
        time: new Date('2022-06-11'),
        checkin: new Date('2022-06-11 8:15:30'),
        checkout: new Date('2022-06-11 17:30:30')
    },
]

renderCICO(2, cicoData)