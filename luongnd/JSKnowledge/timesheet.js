function renderCICO(month, backendData) {
    let year = new Date().getFullYear() // lấy năm hiện tại
    let daysInMonth = new Date(year, month, 0).getDate(); // lấy số ngày trong tháng
    let days = []
    let mm = month < 10 ? `0${month}` : month // thang nho hon 10 thi thanh 01 02
    for (let day = 1; day <= daysInMonth; day++) {

        let dd = day < 10 ? `0${day}` : day // ngay 01 02
        days.push(new Date(`${year}-${mm}-${dd}`)) //
    }
    console.log(days) // lấy được mảng days chứa các ngày trong tháng (yêu cầu 1)

    days = days.map((day) => {
        let cicoRecord = backendData.find((item) => {
            return item.time.getTime() == day.getTime() && !!item.checkin && !!item.checkout // điều kiện lọc chỗ này tùy thuộc logic dự án
        }) // tra ve 1 obj time, ci co

        if (!!cicoRecord) {
            return cicoRecord
        }

        return {
            time: day,
            checkin: null,
            checkout: null,
        }
    })

    console.log(days) // thông tin checkin/checkout trong tháng (yêu cầu 2)
}

let cicoData = [
    {
        time: new Date('2022-02-01'),
        checkin: new Date('2022-02-01 8:15'),
        checkout: new Date('2022-02-01 17:30')
    },
    {
        time: new Date('2022-02-06'),
        checkin: new Date('2022-02-06 8:15'),
        checkout: new Date('2022-02-06 17:30')
    }
] // mock data checkin/checkout get from api

// call function
renderCICO(0, cicoData);