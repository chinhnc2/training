/*Hoisting là hành động mặc định của Javascript, nó sẽ chuyển phần khai báo lên phía
 trên top Trong Javascript, một biến (variable) có thể được khai báo sau khi được sử 
 dụng*/

 //vd:
 console.log(name); //=> undefined
 var name = 200;

//Ta có: Chương trình sẽ tiến hành đưa biến name lên đầu tiên để khai báo tiếp đó nó sẽ
//thực hiện đồng bộ như sau:

var name;
console.log(name);
name = 200;

//Ở dòng 11 name được khai báo những chưa gán giá trị nên dòng 12 log ra undefined

----------------------------------------------------------------
// Có 3 kiểu scope là Global scope, function scope và block scope
// biến Global là biến có thể dùng được ở bất cứ đâu
// biến function scope là biến được khai báo trong function và chỉ được dùng trong đó.
// biến block scope là biến được khai báo trong block {} và chỉ được dùng trong đó.
// Ngoài ra biến var còn có thể là Global variable nếu nó kh đặt trong function scope.
// Khi ta khai báo 1 biến mà không dùng keyword thì tự hiểu nó sẽ là global variable.

//vd
var a = 1;
let b = 2;
const c = 3;

function show() {
	console.log(a); //=> 1
	console.log(b); //=>2
	console.log(c);	//=>3

}

function show1() {
	var d = 1;
	let e = 2;
	const f = 3;
}
console.log(d) //=>err
console.log(e) //=>err
console.log(f) //=>err

var n = 0;
if (n === 0) {
	var x = 1;
	let y = 2;
	const z = 3;
}
console.log(x); // 1
console.log(y); //=> err
console.log(z); //=>err


----------------------------------------------------------------
// Unary Operator
//delete: xóa phần tử trong object

ex: 
delete Math.PI; // returns false (cannot delete non-configurable properties)

const myObj = {h: 4, k: 5};

delete myObj.h; // returns true (can delete user-defined properties)

console.log(myObj); // => {k : 5}

//void: để biểu thị 1 hàm không có giá trị trả về.
//vd:
void function test() {
  console.log('boo!');
  // expected output: "boo!"
}();

//typeOf: để trả về kiểu dữ liệu 
//vd:
var myFun = new Function('5 + 2');
var shape = 'round';
var size = 1;
var foo = ['Apple', 'Mango', 'Orange'];
var today = new Date();

typeof myFun;       // returns "function"
typeof shape;       // returns "string"
typeof size;        // returns "number"
typeof foo;         // returns "object"
typeof today;       // returns "object"
typeof doesntExist; // returns "undefined"

----------------------------------------------------------------
// try Là mối khối block bên trong {}, chúng dùng để thử đoạn code.
// catch là để bắt lỗi nếu như trong block try có lỗi, thì nó sẽ thực thi hàm catch này

//Nếu không có try catch thì khi gặp lỗi nó sẽ làm crash chương trình của chúng ta
console.loging('oke'); //=>err

//Nếu ta dùng try catch thì ta sẽ dễ kiểm soát lỗi trong phần try và bắt lỗi ở phần catch
//để thực hiện các khắc phục thay vì crash app như trên.
//vd

try {
	console.loging('oke');
}
catch(err) {
	//console.log(err);
	console.log('loi cu phap');
}
//=> loi cu phap
