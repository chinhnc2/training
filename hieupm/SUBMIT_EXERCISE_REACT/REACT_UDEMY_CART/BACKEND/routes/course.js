const courseController = require("../controllers/courseController")

const router = require("express").Router();

//* ADD A COURSE
router.post("/" , courseController.addCourse);

//* GET ALL COURSE
router.get("/", courseController.getCourse);

module.exports = router;
