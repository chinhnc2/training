const express = require("express")
const port = 8080;
const cors = require("cors")
const app = express();
const mongoose = require("mongoose")
var bodyParser = require("body-parser")
const morgan = require("morgan")
const dotenv = require("dotenv")

const authorRoute = require("./routes/author")
const courseRoute = require("./routes/course")

dotenv.config();
//* CONNECT DATABASE

mongoose.connect((process.env.MONGODB_URL), () => {
    console.log("Connected to MongoDB");
})


//* -> bodyParser :  hỗ trợ parse dạng JSON về 
app.use(bodyParser.json({ limit: "50mb" }))

app.use(cors())

//* -> morgan : log dưới terminal các dạng request , send API ,. 
app.use(morgan("common"))


//* Khai báo những ROUTES mình sài
app.use("/v1/author", authorRoute)
app.use("/v1/course", courseRoute)


app.listen(port, () => console.log("server is running in port", port));

