//! chứa những controller trong đây
//* lấy data từ model ra 
const { Author, Course } = require("../model/model");

const authorController = {
    //* trong đây bao gồm những controller của author
    //* ADD AUTHOR
    addAuthor: async (req , res) => {
        //* req.body :  JSON gồm những key : value - mình POST lên server
        // res.status(200).json(req.body)
        try{
            //* mình POST lên server  {key - value}  được chấp nhận khi nó bao gồm (like key) trong model Author 
            const newAuthor = new Author(req.body);

            //* Save data vào Database 
            const saveAuthor = await newAuthor.save();

            //* khi save data vào Database thành công -> thì trả về status 200 và data 
            res.status(200).json(saveAuthor);
        }catch(err) {
            //* status 500 : lỗi ở server
            res.status(500).json(err)
        }
    },
    //* GET AUTHOR
    getAuthor: async( req , res) => {
        try{
            //* find() :  tìm tất cả những record có trong DBCollection authorSchema
            const authors = await Author.find();
            res.status(200).json(authors);
        }catch(err){
            res.status(500).json(err);
        }
    }
}

module.exports = authorController;
