import React, { useState, useEffect } from "react";
import PageProduct from "./components/PageProduct";
import Navbar from "./components/navbar";
import Cart from "./components/cart";
// import './styles/resposive.css'
const App = () => {
    const [show, setShow] = useState(true);
    const [cart, setCart] = useState([]);
    console.log('cart current', cart);


    //*thêm 1 sản phẩm vào giỏ hàng
    const handleAddToCart = (item) => {
        let index = cart.findIndex(x => x._id == item._id);
        let items;
        if (index >= 0) { //*Nếu có item trong giỏ hàng thì sẽ tăng nó thêm 1 đơn vị số lượng
            items = [...cart.slice(0, index), { ...item, count: cart[index].count + 1 }]
            setCart(items);
        } else { //*ngược lại thì sẽ thêm nó vào với số lượng là 1
            console.log(item);
            items = [...cart, { ...item, count: 1 }];
            setCart(items);

        }
        // if (cart.indexOf(item) !== -1) return;
        // setCart([...cart, item]);
    };
    //* tăng giảm số lượng
    const handleChange = (item, d) => {
        const ind = cart.indexOf(item);
        const arr = cart;
        arr[ind].count += d;
        if (arr[ind].count === 0) arr[ind].count = 1;
        setCart([...arr]);
    };

    return (
        <React.Fragment>
            <Navbar setShow={setShow} size={cart.length} />
            {show ? (
                <PageProduct handleAddToCart={handleAddToCart} />
            ) : (
                <Cart cart={cart} setCart={setCart} handleChange={handleChange} />
            )}
        </React.Fragment>
    );
};

export default App;