import React, { useState, useEffect } from "react";
// import listCourse from "../data";
import Cards from "./card";
import "../styles/amazon.css";
import axios from 'axios'


const PageProduct = ({ handleAddToCart }) => {

    const [data, setData] = useState([]);
    const [Course, setCourse] = useState([])
    const [keyQuery, setKeyQuery] = useState('');
    const [sortKey, setSortKey] = useState(0);
    const [keyType, setKeyType] = useState('');

    // const [keyStore, setKeyStore] = useState('');
    // const [sortKey, setSortKey] = useState(0);
    // const LIST_LANGUAGE = [
    //     'ReactJS', 'Java', 'C++', 'MySQL', 'Python', 'NodeJS'
    // ]

    //* tìm kiếm course
    const onSearchItem = (e) => {
        setKeyQuery(e.target.value);
        setCourse(searchItem(e.target.value));
    }

    const searchItem = (val) => {
        if (val.length > 0) {
            let searchList = Course.filter(x => x.name.toUpperCase().indexOf(val.toUpperCase()) >= 0);
            return searchList;
        }
        setKeyType('');
        return data;
    }

    // const onSort = (e) => {
    //     if (e.target.value !== sortKey) {
    //         setSortKey(e.target.value);
    //         setCourse(sortByPrice(e.target.value));
    //     } else {
    //         setSortKey(0);
    //         setCourse(data);
    //     }
    // }

    // const sortByPrice = (n) => { // Sắp xếp các sản phẩm theo giá.
    //     let items;
    //     if (n > 0) { // up
    //         items = [...Course]
    //         items = items.sort((a, b) => a.price - b.price);
    //     } else if (n < 0) { // down
    //         items = [...Course];
    //         items = items.sort((a, b) => b.price - a.price);
    //     } else {
    //         setKeyQuery('');
    //         setKeyType('');
    //         items = data;
    //     }
    //     return items;
    // }
    
    useEffect(() => {
        async function getDataFromServer() {
            const res = await axios.get('/v1/course/');
            return res;
        }
        getDataFromServer().then(
            (res) => {
                setData(res.data);
                setCourse(res.data);
            }
        )
    }, [])
    const onChangeSelectType = (e) => {
        // console.log(e.target.value);
        setKeyType(e.target.value);
        setCourse(groupByType(e.target.value));
    }
    const groupByType = (val) => {
        if (val.length > 0) {
            let groupList = Course.filter(x => x.language.toUpperCase() === val.toUpperCase());
            return groupList;
        }
        setKeyQuery('');
        return data; //* data declaration in State
    }
    console.log('list course', Course);

    return (
        <section className="container">
            <div className="row">
                <div className="row search ">
                    <div className='search_item col'>
                        <input type='text' placeholder='Searching a course' value={keyQuery} onChange={onSearchItem} />
                    </div>
                    <div className="group-language col">
                        <input type="text" list="listType" onChange={onChangeSelectType} placeholder='Select Language' value={keyType} />
                        <datalist id="listType" >
                            {/* LIST_LANGUAGE.map((store) => (
                             <option value={store} />
                         )) */}
                            <option value="" />
                            <option value='ReactJS' />
                            <option value='Java' />
                            <option value='MySQL' />
                            <option value='C++' />
                            <option value='Python' />
                        </datalist>
                    </div>
                </div>              
                    {Course.length > 0 && Course.map((item) => (
                        <Cards key={item._id} item={item} handleAddToCart={handleAddToCart} />
                    ))}
            </div>

        </section>
    );
};
export default PageProduct;
