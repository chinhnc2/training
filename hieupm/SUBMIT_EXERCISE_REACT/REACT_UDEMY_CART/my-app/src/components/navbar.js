import React from "react";
import "../styles/navbar.css";

const Navbar = ({ setShow, size }) => {
    return (
        <nav >
            <div className="nav_box">
                <div className="nav_logo" onClick={() => setShow(true)}>
                    <img className="img_logo" src={'https://www.udemy.com/staticx/udemy/images/v7/logo-udemy.svg'} alt="" />
                </div>

                <div className="cart" onClick={() => setShow(false)}>
                    <span>
                        <i className="fas fa-cart-plus"></i>
                    </span>
                    <span>{size}</span>
                </div>
            </div>
        </nav>
    );
};

export default Navbar;
