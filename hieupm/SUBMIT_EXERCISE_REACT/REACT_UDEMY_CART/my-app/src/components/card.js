import React from "react";

const Cards = ({ item, handleAddToCart }) => {
    const { name, author, price, image, publishedDate } = item;

    return (


        <div className="cards col  col-lg-4 col-md-4 col-12 col-sm-6 gy-5">
            <div className="image_box">
                <img src={image} alt="" />
            </div>
            <div className="details" style={{ marginLeft: '6px', }}>
                <p className="course-name">{name}</p>
                <p className="course-author">Author: {author}</p>

                <div className="rate" >
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <p className="rate-num" style={{ fontSize: '14px', color: '#b4690e', }}>4.5</p>
                        <div className="rate-start">
                            <i className="fa-solid fa-star rate-start-icon"></i>
                            <i className="fa-solid fa-star rate-start-icon"></i>
                            <i className="fa-solid fa-star rate-start-icon"></i>
                            <i className="fa-solid fa-star rate-start-icon"></i>
                            <i className="fa-solid fa-star-half rate-start-icon"></i>
                        </div>
                        <p className="rate-num" style={{ fontSize: '12px' }}>(241)</p>
                    </div>

                    <div>
                        <p className="course-date" style={{ fontSize: '12px', color: 'grey', fontWeight: '500', }}>{publishedDate}</p>
                    </div>

                </div>
                <p className="course-price">${price}</p>
                <button onClick={() => handleAddToCart(item)}>Add to Cart</button>
            </div>
        </div>




    );
};

export default Cards;


