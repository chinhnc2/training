import React, { useState, useEffect } from "react";
import "../styles/cart.css";

const Cart = ({ cart, setCart, handleChange }) => {
    const [price, setPrice] = useState(0);


    const handleRemove = (item) => { //* xóa item được chọn trong giỏ hàng

        let index = cart.findIndex(x => x._id == item._id)
        let items = [...cart.slice(0, index), ...cart.slice(index + 1)];

        setCart(items);
    };

    const totalPrice = () => {
        let sum = 0;
        cart.map((item) => (sum += item.count * item.price));
        setPrice(sum);
    };

    useEffect(() => {
        totalPrice();
    });

    const handleClearAll = () => {
        setCart([]);
    }
    return (
        <article>
            {cart.length > 0 && cart.map((item) => (
                <div className="cart_box" key={item._id}>
                    <div className="cart_img">
                        <img src={item.image} alt="" />
                        <p>{item.name}</p>
                    </div>
                    <div>
                        <button className="btn btn-success" onClick={() => handleChange(item, 1)}>+</button>
                        <button>{item.count}</button>
                        <button className="btn btn-danger" onClick={() => handleChange(item, -1)}>-</button>
                    </div>
                    <div >
                        <span>{item.price}</span>
                        <button className="btn" onClick={() => handleRemove(item)}>Remove</button>
                    </div>
                </div>
            ))}
            <div className="total">
                <span>Total Price of your Cart</span>
                <span style={{ padding: '16px' }}>{price}$</span>
                <div>
                    <button className="btn btn-primary" onClick={() => handleClearAll()}>Clear All</button>

                </div>  
            </div>
        </article>
    );
};

export default Cart;