//! viết các route , /api/...

const authorController = require("../controllers/authorController");

const router = require("express").Router();

//* route ADD AUTHOR
router.post("/" , authorController.addAuthor )

//* route GET AUTHOR 
router.get("/" , authorController.getAuthor )

module.exports = router;