//* Tạo khung cho Database , chứa gì trong đó , .. 

const mongoose = require("mongoose")

const authorSchema = new mongoose.Schema({
    name: {
        type: String,
        required : true
    },
    year: {
        type: Number,
        required : true
    },
    //* courses : sẽ liên kết tới coursesSchema bên dưới , ràng buộc  : 1 tác giả có nhìu course , nhưng 1 course thì có 1 tác giả
    courses: [{
        type: mongoose.Schema.Types.ObjectId,
        //* ref : liên kết tới coursesSchema để lấy thông tin khóa học của tác giả đó -> Course = mongoose.model("Course", coursesSchema) , bên dưới
        ref: "Course"
    }],
});


const coursesSchema = new mongoose.Schema({

    //!required :  điều kiện bắt buộc
    name: {
        type: String,
        required: true
    },
    publishedDate: {
        type: String ,
        required : true 
    },
    price: {
        type: Number ,
        required : true
    },
    language: {
       type : String,
       required : true ,
    },
    image : {
        type : String,
    },
    author: {
        //* mongoose.Schema.Types.ObjectId : create _id : 122jdnasdk123nndasd
        type: mongoose.Schema.Types.ObjectId,
        ref: "Author"
    }
});

//* Tạo model cho nó
//* Lưu ý: khi đặt tên cho model "name" như nào thì ref : "name" phải trùng "name" như thế !
let Course = mongoose.model("Course", coursesSchema)
let Author = mongoose.model("Author" , authorSchema)

//* Export DB 
module.exports = { Course , Author }