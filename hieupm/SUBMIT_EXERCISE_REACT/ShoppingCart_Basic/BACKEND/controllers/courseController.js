const {Author , Course } =  require("../model/model")

const courseController = {

    //* ADD COURSE
    addCourse : async (req , res) => {
        try{
            const newCourse = new Course(req.body);
            const saveCourse = await newCourse.save();
            //* khi phía client truyền key author : value(ObjectID) post lên  ->  bên DB Author sẽ nhận được data của Course đó ở dạng ObjectOID
            //* 1 logic : nếu truyền vào author
            if(req.body.author){
                //* 2 tìm author để thêm course vào -> courses : []  trong DB  của author đó
                     //* -> tìm ở trong DB Author có _id match với req.body.author
                const author = Author.findById(req.body.author)
                //* 3 khi tìm thấy _id match với req.body.author -> update course vào field của DB đó
                    //* 1 course chỉ có 1 author thôi ->  updateOne()
                    //* update course vào field courses của DB Author
                await author.updateOne({ $push : { courses : saveCourse._id } });
            }

            res.status(200).json(saveCourse);

        }catch(err){
            res.status(500).json(err);
        }
    },

    //* GET ALL COURSE
    getCourse : async (req , res) => {
        try{
            console.log(true)
            const courses = await Course.find();
            console.log(courses);
            res.status(200).json(courses)
        }catch(err){
            res.status(500).json(err)
        }
    }
}

module.exports = courseController;