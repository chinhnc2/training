var jsons = [
    {
        time: 5,
        checkIn: 1645525689097,
        checkOut: 1645525742336
    },
    {
        time: 10,
        checkIn: 1645525706832,
        checkOut: 1645525758920
    },
    {
        time: 14,
        checkIn: 1645525724569,
        checkOut: 1645525774743
    },
    {
        time: 20,
        checkIn: 1645424597980,
        checkOut: 1645425143574
    },
    {
        time: 27,
        checkIn: 1645425585843,
        checkOut: 1645425586979
    }
];



function renderCheckInfos(months, jsons) {

    var days = [];
    var date = new Date(2022, months - 1, 1);


    while (date.getMonth() === months - 1) {
        var objTime = {};
        var day = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
        objTime.day = day;
        var timeTrue = jsons.find((json) => {
            return date.getDate() === json.time;
        })

        if (timeTrue !== undefined) {
            objTime.checkIn = timeTrue.checkIn;
            objTime.checkOut = timeTrue.checkOut;
        }
        
        else {
            objTime.checkIn = null;
            objTime.checkOut = null;
        }// ngược lại nếu không có thì checkIn checkOut = null
        days.push(objTime);
        date.setDate(date.getDate() + 1);
    }
    return days;



    //................................................................

    // return về một mảng có số phần tử là số ngày trong tháng từ 1 tới
    // Trong mỗi phần tử có dữ liệu checkin và checkout
}
console.log(renderCheckInfos(3, jsons))

// function renderCICO(month, backendData) {
//     let year = new Date().getFullYear() // lấy năm hiện tại
//     let daysInMonth = new Date(year, month, 0).getDate(); // lấy số ngày trong tháng
//     let days = []
//     let mm = month < 10 ? `0${month}` : month
//     for (let day = 1; day <= daysInMonth; day++) {
//         let dd = day < 10 ? `0${day}` : day
//         days.push(new Date(`${year}-${mm}-${dd}`))
//     }
//     console.log(days) // lấy được mảng days chứa các ngày trong tháng (yêu cầu 1)
    
    
//     var html = "";
//     days = days.map((day) => {
//         let cicoRecord = backendData.find((item) => {
//             return item.time.getTime() == day.getTime() && !!item.checkin && !!item.checkout // điều kiện lọc chỗ này tùy thuộc logic dự án
//         })

//         if (!!cicoRecord) {
//             return cicoRecord
//         }

//         return {
//             time: day,
//             checkin: null,
//             checkout: null,
//         }
//     })
//     console.log(cicoData)
//     console.log(days) // thông tin checkin/checkout trong tháng (yêu cầu 2)
// }

 // mock data checkin/checkout get from api

// call function
// renderCICO(2, cicoData);


