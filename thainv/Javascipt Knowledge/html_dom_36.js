//HTML DOM: là Document Object Model, là cây HTML object, nó bao gồm tất cả các thẻ HTML
// các attribute của chúng, các method có thể truy cập đến các thẻ HTML, các sự kiện
// của các thẻ HTML

// HTML Dom giúp chúng ta có thể thay đổi nội dung, attribute của 1 thẻ HTML, cũng như
// thay đổi các thuộc tính CSS. Nó giúp ta có thể dễ dàng tương tác với các sự kiện
// trên các thẻ HTML.