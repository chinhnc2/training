// function được định nghĩa với 1 từ function ở đầu và phía sau là tên hàm
//vd:
function sum(params) {
	return params;
}
//function ở đây có thể là void nếu như trong thân hàm không có câu lệnh return;
// Ngoài ra ta có thể khai báo 1 biến và gán nó bằng 1 function
const tong = function(params) {
	return params + 10;
}
var x = tong(4);

// IIFE: ta có thể gọi nhanh hàm bằng () sau đuôi 1 function mà kh cần phải gọi để
// sử dụng;
//vd:
(function () {
  /* ... */
})();
