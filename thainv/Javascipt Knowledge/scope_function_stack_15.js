//vd:
function daily() {
	homeWork();
}
function homeWork() {
	console.log('Làm bài tập');
	console.log('Nộp bài tập');
}
daily();

//Đầu tiên CallStack sẽ có 1 hàm main lớn ở ngoài cùng, sau đó ta gọi hàm daily() thì 
// hàm daily sẽ được đưa vào callStack kế tiếp trong hàm daily nó sẽ thực hiện các code
// bên trong và sẽ đưa hàm homeWork vào callStack. Sau đó, nó sẽ thực hiện các code
// bên trong hàm homeWork và hàm homeWork sẽ được back ra khỏi callStack, sau khi hàm
// homeWork thực hiện xong nếu như trong daily cũng đã thực hiện xong thì nó cũng sẽ back
// ra khỏi CallStack và cuối cùng chỉ còn lại hàm main ngoài cùng chương trình. Nếu chương
// trình kết thúc thì hàm main cũng back ra theo.

//CallStack
//main() -> daily() -> homeWork() -> daily() -> homeWork() -> main();

//vd:
function foo(i) {
  if (i < 0)
    return;
  console.log('begin: ' + i);
  foo(i - 1);
  console.log('end: ' + i);
}
foo(3);
// main() -> foo(3) -> foo(2) -> foo(1) -> foo(0) -> foo(1) -> foo(2) -> foo(3) -> main()
// Output:

// begin: 3
// begin: 2
// begin: 1
// begin: 0
// end: 0
// end: 1
// end: 2
// end: 3
