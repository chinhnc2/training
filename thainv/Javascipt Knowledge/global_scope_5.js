//Là phạm vi toàn cục
//Nếu biến ở trong global scope thì nó sẽ được sử dụng cho cả chương trình.
// Nếu ta khai báo 1 biến bên ngoài function hay block thì nó sẽ thuộc global scope
// và là biến global có thể dùng cho toàn chương trình
// Nếu như ta khai báo mà không dùng các key như "var, let, const" thì biến đó sẽ nằm
// trong phạm vị global.
// Trong HTML, phạm vi toàn cục là đối tượng window.
// Nếu như ta khai báo biến var không phải trong function thì nó cũng sẽ có phạm vi hoạt
// động là global.