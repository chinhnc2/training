//Là một khối Block được bao bọc bên trong { ... } với các nhãn như If hay For, ....
//vd:
var x = 1;
let y = 1;

if (true) {
  var x = 2; //Sẽ bị lỗi nếu sử dụng Strict mode
  let y = 2;
}

console.log(x);
// expected output: 2

console.log(y);
// expected output: 1

//Biến let khi được khai báo trong block thì chỉ có thể hoạt động trong phạm vi block
// không thể sử dụng ở bên ngoài được.
// Còn biến var khi được khai báo trong block thì nó vẫn như là 1 global variable có thể
// sử dụng ở bất cứ đâu.
