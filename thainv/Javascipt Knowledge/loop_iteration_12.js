//for (khai báo, điều kiện, lệnh thay đổi biến lặp sau mỗi vòng lặp)
for (let i = 0; i < 10; i++) {
	console.log(i) // => 0 1 2 3 4 5 6 7 8 9
}
//đầu tiên nó sẽ tạo ra biến i = 0, nó sẽ kiểm tra i có thỏa điều kiện không, nếu không thỏa
// thì sẽ break ngừng vòng lặp, nếu không thì nó sẽ đi đến lệnh i++ tăng biến i lên 1 đơn vị
// Ngoài ra ta có thể dùng break để dừng vòng lặp hoặc continue để bỏ qua 1 vòng lặp để tiến
// đến giá trị tiếp theo.
//for (key in obj) lấy ra các key bên trong obj
//for (index in array) nó sẽ lấy ra các index của array;
a = [1, 2, 3];
for (let index in a) {
	let sum = 0;
	sum += a[index];
	return sum //=> 6
}

//for (x of array) thì x sẽ là tất cả các phần tử bên trong array hay string.
for (let x of a) {
	console.log(x) //=> 1 2 3
}
let b = 'string'
for (let x of b) {
	console.log(b) //=> 's' 't' 'r' 'i' 'n' 'g'
}
// ngoài ra ta có array.forEach cũng tương tự như for x of array
a.forEach((x) => {
	console.log(x);
}) = for (let x of a) {
	console.log(x);
}

//while: đầu tiên ta khởi tạo biến i và sau đó đặt điều kiện trong hàm while 
// mỗi khi biến i thay đổi thì nó sẽ kiểm tra có thỏa điều kiện không, nếu không thì
// vòng lặp sẽ break, dừng. Ngoài ra nó cũng có break và continue như for loop
let i = 0;

while (i < 10) {
  console.log("The number is " + i);
  i++;
}
// Nếu như trong vòng lặp while mà không có lệnh thay đổi giá trị của i (ví dụ như i++)
// thì vòng lặp này sẽ chạy vô hạn.
//do...while
do {
  console.log("The number is " + i);
  i++;
}
while (i < 10);
//Cũng tương tự như while nhưng đây là thực hiện trước rồi mới xét điều kiện, còn đối
// với while thì nó sẽ luôn xét điều kiện trong vòng lặp while, bất cứ khi nào không thỏa
// điều kiện thì sẽ đều break;