//Regex: là một mẫu được sử dụng để kiểm tra, so khớp với các kí tự trong 1 xâu.
//Regex còn có 1 vài method hỗ trợ như match(), matchAll(), replace(), replaceAll(),
// search(), split(), exec(), test().
//vd khởi tạo:
const re = /ab+c/;
//or
const re = new RegExp('ab+c');

// Đây là link cheat sheet dùng để tìm các kí tự phù hợp với yêu cầu:
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions/Cheatsheet

const re = /\w+\s/g; // Đầu tiên lấy các từ -> dấu cách thì dừng \w+ là để lấy nhiều từ, \s là space
const str = 'fee fi fo fum';
const myArray = str.match(re);
console.log(myArray); // ["fee ", "fi ", "fo "]


const paragraph = 'The quick brown fox jumps over the lazy dog. It barked.';
const regex = /[A-Z]/g;
const found = paragraph.match(regex);

console.log(found); // expected output: Array ["T", "I"]


//
const str = 'fee fi fo fum';
const re = /\w+\s/g;

console.log(re.exec(str)); // ["fee ", index: 0, input: "fee fi fo fum"]
console.log(re.exec(str)); // ["fi ", index: 4, input: "fee fi fo fum"]
console.log(re.exec(str)); // ["fo ", index: 7, input: "fee fi fo fum"]
console.log(re.exec(str)); // null


//replace()
const p = 'The quick brown fox jumps over the lazy dog. If the dog reacted, was it really lazy?';

console.log(p.replace('dog', 'monkey'));
// expected output: "The quick brown fox jumps over the lazy monkey. If the dog reacted, was it really lazy?"


const regex = /Dog/i;
console.log(p.replace(regex, 'ferret'));
// expected output: "The quick brown fox jumps over the lazy ferret. If the dog reacted, was it really lazy?"


//test():
const str = 'hello world!';
const result = /^hello/.test(str);

console.log(result); // true