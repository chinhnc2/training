// Promise là 1 Object trong Javascript để hỗ trợ xử lý bất đồng bộ, nó sẽ đợi khi
// logic code thực hiện xong thì nó mới đến các logic code khác.
// Promise có 3 trạng thái: pending, fullfilled, reject
// vd: syntax Promise
let myPromise = new Promise(function(myResolve, myReject) {

  //logic code


  //myResolve(5); // when successful
  myReject('err');  // when error
});

// "Consuming Code" (Must wait for a fulfilled Promise)
myPromise
.then((value) => {
  console.log(value);
  //logic khi đã thành công
})
.catch((err) => {
  console.log(err);
  // logic khi thất bại ( có lỗi );
})
.finally(() => {
  //Chỉ cần Promise trên chạy myResolve hay myReject thì nó đều thực hiện finally
})

// đầu tiên ta gọi new 1 obj là Promise với 1 callBack excutor có 2 tham số truyền vào là 2 callBack:
// 1. myResolve: Khi logic code thành công thì ta dùng myResolve và nó sẽ thực thi hàm
// myPromise.then bên dưới
// 2. myReject: tương tự như trên khi logic code thất bại hay bị lỗi.
