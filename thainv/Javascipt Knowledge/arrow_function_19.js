// Arrow function là 1 cách để rút ngắn gọn function, làm cho code dễ đọc dễ xử lý.
//vd:
const a = () => {
	console.log(10);
}
a();

//thay vì
const a = function() {
	console.log(10);
}
a();

// Nó có thể return về giá trị ngắn gọn hơn so với function thông thường
const a = () => 10;
console.log(a());
//thay vì
const a = function() {
	return 10;
}
console.log(a());
// Hoặc nếu function chỉ có 1 tham số thì ta có thể bỏ dấu ngoặc bên ngoài tham số
const a = x => x;
console.log(a(5));