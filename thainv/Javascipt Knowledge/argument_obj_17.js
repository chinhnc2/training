//Argument Object là các tham số được truyền vào hàm có thể là 1 hoặc nhiều.
// Chúng được gộp lại tạo thành 1 Object với các key là index trong parameters.
// Ta có thể dễ dàng gọi và lấy chúng ra cũng như spread để chuyển chúng thành Array
// không cần phải lấy các tham số truyền vào như cách lấy các parameter thông thường.
// Có thể ta không biết function này sẽ nhận bao nhiêu tham số nên đây là cách tốt nhất
// để lấy hết các tham số mà không bị lỗi.
function longestString() {
	console.log(arguments); //=>  { '0': 'stringka', '1': 'memo', '2': 'longer', '3': 'do' }
	console.log([...arguments]) // => ['stringka', 'memo', 'longer', 'do']
  var longest = '';
  for (var i=0; i < arguments.length; i++) {
    if (arguments[i].length > longest.length) {
      longest = arguments[i];
    }
  }
  return longest;
}
console.log(longestString('stringka', 'memo', 'longer', 'do'))