//String operator: Ngoài các toán tử so sánh các string thì string còn có 1 toán tử khác
// là + để nối 2 chuỗi giá trị với nhau
//vd:
console.log('my ' + 'string'); //'my String' 
console.log('my ' + '7'); // 'my 7' 

// Comma Operator: Toán tử dấu phẩy (,) là toán tử để đánh giá mỗi toán tử bên trong
// và trả về giá trị nằm ngoài cùng bên tay phải. 
// Nó sẽ thực hiện các lệnh trong (,) nhưng chỉ trả về giá trị nằm bên phải ngoài cùng
//vd: 
function myFunc() {
  var x = 0;

  return (x += 1, x); // the same as return ++x;
}

for (var i = 0, j = 9; i <= 9; i++, j--)
  console.log('a[' + i + '][' + j + '] = ' + a[i][j]);

/*
a[0][9]= 9
a[1][8]= 8
a[2][7]= 7
a[3][6]= 6
a[4][5]= 5 */

var x, y, z;

x = (y = 5, z = 6); // Returns 6 in console
console.log(x); // 6
console.log(y); // 5
console.log(z); // 6

