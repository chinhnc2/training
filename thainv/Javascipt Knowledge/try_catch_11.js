// try Là mối khối block bên trong {}, chúng dùng để thử đoạn code.
// catch là để bắt lỗi nếu như trong block try có lỗi, thì nó sẽ thực thi hàm catch này

//Nếu không có try catch thì khi gặp lỗi nó sẽ làm crash chương trình của chúng ta
console.loging('oke'); //=>err

//Nếu ta dùng try catch thì ta sẽ dễ kiểm soát lỗi trong phần try và bắt lỗi ở phần catch
//để thực hiện các khắc phục thay vì crash app như trên.
//vd

try {
	console.loging('oke');
}
catch(err) {
	//console.log(err);
	console.log('loi cu phap');
}
//=> loi cu phap