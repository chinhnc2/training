//Object: đại diện cho 1 kiểu dữ liệu trong JS. Nó được sử dụng để lưu trữ các bộ 
// key - value.
//Constructor: Object(): tạo 1 obj mới. Nó là 1 hàm để khởi tạo obj.
//Object.assign(target, source): Nó dùng để lấy những giá trị của obj source và nối
// vào obj target, nếu có các key trùng thì nó sễ lấy value của source
const obj = { a: 1 };
const copy = Object.assign({}, obj);
console.log(copy); // { a: 1 }

// Nhưng nó cũng sẽ làm thay đổi obj target.
const target = { a: 1, b: 2 };
const source = { b: 4, c: 5 };

const returnedTarget = Object.assign(target, source);

console.log(target);
// expected output: Object { a: 1, b: 4, c: 5 }

console.log(returnedTarget);
// expected output: Object { a: 1, b: 4, c: 5 }

//Object.entries(obj): nó sẽ trả về các cặp key - value của các element trong obj
//Object.keys và Object.values cũng lần lượt trả về keys và values.

// Ngoài ra:
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object?retiredLocale=vi


//Class: là 1 khuông mẫu để tạo đối tượng.
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes?retiredLocale=vi