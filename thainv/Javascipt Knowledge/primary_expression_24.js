// this trong javascript sẽ có rất nhiều trường hợp:
// Nếu như trong 1 obj method thì this sẽ tham chiếu đến obj đó
// Nếu như nằm trong function thì nó có thể tham chiếu đến global Object như window
// nhưng nếu áp dụng Strict mode thì nó sẽ là undefined
// Trong 1 sự kiện trong DOM thì nó sẽ tham chiếu đến element nhận sự kiện này.
// Ngoài ra các method như call(), apply(), bind() có thể tham chiếu đến this ở bâts
// Nếu như nằm bên ngoài những cái trên thì nó sẽ là 1 global Object như window trong browser
// kì đối tượng nào

//vd:
const person = {
  firstName: "John",
  lastName: "Doe",
  id: 5566,
  fullName : function() {
    return this.firstName + " " + this.lastName;
  }
};
console.log(person.fullName()); // => John Doe

function myFunction() {
  return this;
}
console.log(myFunction()); // => global Object

//HTML dom: ở đây this sẽ là button và khi click nó sẽ set display none là mất
//<button onclick="this.style.display='none'">Click to Remove Me!</button>

//call() and apply(: nó sẽ sử dụng phương thức của đối tượng này nhưng lại gán vào
// 1 đối tượng khác để sử dụng, các từ khóa this sẽ tham chiếu tới đối tượng được
// truyền vào trong call();
const person1 = {
  fullName: function() {
    return this.firstName + " " + this.lastName;
  }
}

const person2 = {
  firstName:"John",
  lastName: "Doe",
}

// Return "John Doe":
person1.fullName.call(person2); // thay call bằng apply và diễn ra tương tự

//bind: Ở đây nó khác với call và apply là nó mượn 1 method của đối tượng khác và
// khi ta muốn thực hiện thì ta cần phải tiến hành gọi hàm nữa mới được;
const person = {
  firstName:"John",
  lastName: "Doe",
  fullName: function () {
    return this.firstName + " " + this.lastName;
  }
}

const member = {
  firstName:"Hege",
  lastName: "Nilsen",
}

let fullName = person.fullName.bind(member);
console.log(fullName()); //Hege Nilsen
