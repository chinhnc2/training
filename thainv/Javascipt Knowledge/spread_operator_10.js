// Dùng để trải 1 Array hay Object ra nhưng vẫn đảm bảo tính immutable.

//vd:
let a = [1, 2, 3];
console.log(...a) //1 2 3;
console.log([...a, 5]) //[1, 2, 3, 5];
console.log(a) // [1, 2, 3] immutable
console.log([...a.slice(0, 2)]) // [1,2]
console.log(a) // [1, 2, 3] immutable

//
let obj1 = { foo: 'bar', x: 42 };
let obj2 = { foo: 'baz', y: 13 };

let clonedObj = { ...obj1, id: 15}; 
console.log(clonedObj) // Object { foo: "bar", x: 42, id: 15 }



let mergedObj = { ...obj1, ...obj2 }; // Object { foo: "baz", x: 42, y: 13 }
console.log(mergedObj); 
console.log(obj1); // { foo: 'bar', x: 42 }; immutable
console.log(obj2); // { foo: 'baz', y: 13 }; immutable
