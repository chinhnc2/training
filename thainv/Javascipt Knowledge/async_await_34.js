//Async await: Tương tự như Promise nhưng khi thực hiện với async await thì sẽ dễ đọc
// dễ hiểu hơn.

async function myDisplay() {
  console.log('let do it');
  let myPromise = new Promise(function(resolve) {
    console.log('kkk');
    setTimeout(function() {
      resolve("I!!");
    }, 3000);
  });
  let a = await myPromise;
  console.log(a);
  console.log(100);
  console.log('done')
}

myDisplay();

// let do it
// kkk
// I
// 100
// done

// Những lệnh bên dưới await thì phải đợi nó thực thi xong thì nó mới có thể chạy được
// tương tự như việc các lệnh bên dưới sẽ nằm trong hàm then() trong promise.