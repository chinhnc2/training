// Các method quan trọng trong Array:

// - Array.length: trả về số phần tử trong mảng.
// - Array.fill(n): dùng để điền số n vào 1 mảng với số phần tử là Array.length
// - Array.concat(array1): dùng để nối 2 array với nhau là Array và array1.
// vd:
const array1 = ['a', 'b', 'c'];
const array2 = ['d', 'e', 'f'];
const array3 = array1.concat(array2); //=> ["a", "b", "c", "d", "e", "f"]

// - Array.entries(): return Array Iterator bao gồm các cặp key/value là index và giá trị trong mảng đó
// vd:
const a = ['a', 'b', 'c'];

for (const [index, element] of a.entries())
  console.log(index, element);

// 0 'a'
// 1 'b'
// 2 'c'

// - Array.find(x => ...): Method trả về phần tử đầu tiên trong mảng thỏa điều kiện ở (...);
//vd:
let a = [{a: 'a'}, {a:'b'}, {a: 'a'}, {a:'c'}];
let y = a.find(x => x.a === 'a') // y = {a: 'a'};
// Nó sẽ trả về phần tử đầu tiên trong mảng có điều kiện trùng với x.a === 'a';
// - Array.findIndex(): tương tự Array.find nhưng nó sẽ trả về vị trí của phần tử thỏa mãn
// điều kiện đó trong mảng.

// - Array.filter(x => ...): Method trả về 1 mảng gồm các phần tử thõa mãn điều kiện ở (...)
// vd:
let a = [{a: 'a'}, {a:'b'}, {a: 'a'}, {a:'c'}];
let y = a.filter(x => x.a === 'a') // y = {a: 'a'};
console.log(y) //=> [{a: 'a'}, {a: 'a'}];

// - Array.forEach((x) {}): Tương tự như for x of Array, method này dùng để lặp qua mọi
// phần tử trong mảng.

// - Array.includes(n): Dùng để kiểm tra xem mảng cso chứa số n không (True or False)
// vd:
let a = [1, 2, 3, 4, 5];
console.log(a.includes(4)); // => true

// - Array.indexOf(n); Tương tự như includes là để kiểm tra xem mảng có số n không nhưng
// nó sẽ trả về index của phần tử đó trong mảng, nếu không có sẽ trả về -1 còn nếu có sẽ 
// là từ 0 -> Array.length - 1
//vd:
let a = [1, 2, 3, 4, 5];
console.log(a.indexOf(4)); // => 3

// -Array.join(): Dùng để nối các phần tử trong mảng và trả về 1 string
//vd:
var a = ['Wind', 'Water', 'Fire'];
a.join();      // 'Wind,Water,Fire'
a.join(', ');  // 'Wind, Water, Fire'
a.join(' + '); // 'Wind + Water + Fire'
a.join('');    // 'WindWaterFire'

// - Array.map(x => ...): Convert các phần tử trong mảng và trả về mảng mới với lệnh biến
// đổi trong (...)
//vd:
	const array1 = [1, 4, 9, 16];

	// pass a function to map
	const map1 = array1.map(x => x * 2); // convert mảng thành 1 mảng với các phần tử *2

	console.log(map1);
	// expected output: Array [2, 8, 18, 32]

// - Array.pop(): loại 1 phần tử ở cuối cùng ra khỏi mảng
// - Array.shift(): loại 1 phần tử ở đầu mảng ra ngoài.
// - Array.push(): thêm 1 phần tử vào cuối cùng mảng
// - Array.reduce(): Dùng để giảm các phần tử trong mảng, có thể là 1 giá trị đơn lẻ như tính tổng
// vd:
const array1 = [1, 2, 3, 4];

// 0 + 1 + 2 + 3 + 4
const initialValue = 0;
const sumWithInitial = array1.reduce(
  (previousValue, currentValue) => previousValue + currentValue,
  initialValue
);

console.log(sumWithInitial);
// expected output: 10

// - Array.reverse(); trả về 1 mảng được đảo ngược vị trí.
// - Array.slice(start) / Array.slice(start, end): Dùng để cắt mảng và show ra 1 mảng từ
// start -> end - 1 hoặc từ start -> Array.length - 1
// vd:
const animals = ['ant', 'bison', 'camel', 'duck', 'elephant'];

console.log(animals.slice(2));
// expected output: Array ["camel", "duck", "elephant"]

console.log(animals.slice(2, 4));
// expected output: Array ["camel", "duck"]

console.log(animals.slice(1, 5));
// expected output: Array ["bison", "camel", "duck", "elephant"]

// - Array.splice(): Dùng dể xóa hoặc thêm 1 hoặc nhiều phần tử vào 1 vị trí nào đó
// syntax: Array.splce(index) / Array.splice(index, deletecount) / Array.splice(index, deleteCount, item1)
//vd:
let myFish = ['angel', 'clown', 'drum', 'mandarin', 'sturgeon']
let removed = myFish.splice(3, 1)

// myFish is ["angel", "clown", "drum", "sturgeon"]
// removed is ["mandarin"]

let removed = myFish.splice(2, 1, 'trumpet')

// myFish is ["angel", "clown", "trumpet", "sturgeon"]
// removed is ["drum"]


//- Array.sort(): Dùng để sắp xếp 1 mảng theo thứ tự tăng dần hoặc custom
//vd:
const months = ['March', 'Jan', 'Feb', 'Dec'];
months.sort();
console.log(months);
// expected output: Array ["Dec", "Feb", "Jan", "March"]

const array1 = [1, 30, 4, 21, 100000];
array1.sort();
console.log(array1);
// expected output: Array [1, 100000, 21, 30, 4]
//
var numbers = [4, 2, 5, 1, 3];
numbers.sort(function(a, b) {
  return a - b;
});
console.log(numbers);

// [1, 2, 3, 4, 5]
