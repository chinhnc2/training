//Array
let a = [1, 2, 3, 4, 5];
let index = 1 // index from 0 -> a.length - 1;
console.log(a[index])
//Object
let b = {id: 1};
console.log(b['id']);
console.log(b.id);