// var: Có thể được sử dụng như một biến Global nếu như không được khai báo trong 1 function
// let: Là biến global nếu được đặt bên ngoài, còn nếu như đặt trong block hay function
// thì nó sẽ là 1 biến cục bộ không thể sử dụng được bên ngoài. Khi đã khai báo 1 biến let
// thì nó sẽ kh được khai báo lại nữa.
// const: Tương tự như biến let, có điều nó là 1 hằng số không thể thay đổi được.

var a = 1;
let b = 2;
const c = 3;

function show() {
	console.log(a);
	console.log(b);
	console.log(c);

	a = 2;
	b = 3;
	c = 4; // => err;

	var a = 5;
	let b = 10; //err;
} 