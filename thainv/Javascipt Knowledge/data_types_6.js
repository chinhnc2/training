//Trong Js có các kiểu data như: Number, String, Boolean, Null, Undefined, BigInt, Symbol, Objects
//Boolean: True or False
//Number: Int or Float
//String: //Dữ liễu kiểu chuỗi xâu, gồm nhiều kí tự của bảng mã ASCII
//Null: có nghĩa là giá trị rỗng hoặc giá trị không tồn tại, nó có thể được sử dụng để 
//gán cho một biến như là một đại diện không có giá trị.
//undefined: Biến đã được khai báo nhưng chưa đưuọc gán giá trị.
//BigInt: Tương tự như Number nhưng là những số bên ngoài phạm vi của Int
//SymBol: là kiểu dữ liệu duy nhất, không thể thay đổi 
//Objects: Là một đối tượng gồm key và value được bỏ trong dấu {}, key là các String hoặc Symbol value.
// value có thể là các type tùy ý
// Ngoài ra nếu không phải là biến const thì ta có thể thay đổi các value với các datatype
// tùy ý không như các ngôn ngữ khác.
//vd:
let a = false; //Boolean
let b = 123 // Number
let c = '123123' //String
let d = null; //Null
let e = undefined; //Undefined
let f =  const x = 2n ** 53n; //9007199254740992n => BigInt
let g = Symbol('k') ;
let h = Symbol('k');
console.log(g === h) // => false
let i = {id: 1};
i = 1000;
console.log(i)