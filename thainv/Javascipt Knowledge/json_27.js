// JSON là 1 kiểu lưu trữ và truyền tải thông tin, nó thường được dùng để tải thông tin api, ..
// JSON nghĩa là Javascript Object Notation
// JSON giống với js obj về mặt cú pháp bao gồm key và value nhưng nó lại là kiểu string.
// JSON yêu cầu các key và value phải là dấu ""
//vd JSON obj:
{"firstName":"John", "lastName":"Doe"}
//vd JSON array:
"employees":[
  {"firstName":"John", "lastName":"Doe"},
  {"firstName":"Anna", "lastName":"Smith"},
  {"firstName":"Peter", "lastName":"Jones"}
]

//để ta có thể sử dụng được JSOn ta cần phải convert từ JSON sang js Object:
// nó sẽ convert từ string sang 1 kiểu dữ liệu khác.
let text = '{"name": "thai", "age": "15"}';
console.log(typeof text); // => string
let parseText = JSON.parse(text);

console.log(typeof parseText); // => obj

//
JSON.parse('{}');              // {}
JSON.parse('true');            // true
JSON.parse('"foo"');           // "foo"
JSON.parse('[1, 5, "false"]'); // [1, 5, "false"]
JSON.parse('null');            // null