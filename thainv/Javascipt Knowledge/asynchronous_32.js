// Asynchronous: Thay vì đồng bộ chạy code từ trên xuống dưới thì nó sẽ tiến hành
// 1 cách bất đồng bộ, sẽ trì hoãn 1 sự kiện nào đó (chẳng hạn như setTimeout).
//vd: 
const show = () => {
  console.log(1);
  setTimeout(() => {
    console.log('middle')
  }, 2000);
  console.log(3);
}
show();

// Khi gọi hàm show() thì đầu tiên nó sẽ chạy dòng số 5 tiếp đó sẽ chạy dòng số 6 nhưng
// vì setTimeOut là 1 hàm bất đồng bộ nên nó sẽ chờ đến khi hoàn thành và nó sẽ next
// sang lệnh tiếp theo là dòng số 9, đến khi callStack trống rồi và thời gian đợi đã
// đủ thì nó sẽ đưa callBack bên trong setTimeOut thực hiện/

//vd:
const show = () => {
  console.log(1);
  setTimeout(() => {
    console.log('middle')
  }, 2000);
  for (let x of Array(100000).fill(5)) {
    console.log(x);
  }
  console.log(3);
}
show();
// Nó sẽ chạy xong vòng lặp for để loại hết ra khỏi call Stack rồi nó mới chạy callback
// bên trong setTimeOut mặc dù thời gian đã đủ.