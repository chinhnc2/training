//Function scope là phạm vị trong 1 function.
// Tất cả mọi biến được khai báo trong funciton scope thì đều là biến cục bộ chỉ sử dụng
// được trong function đó, trừ khi khai báo không có keyword như "var, let, const" nhưng 
// như vậy sẽ phá vỡ quy tắc strict mode.

//vd:
show();
function show() {
	var a = 0;
	let b = 1;
	const c = 2;

	console.log(a, b, c); // => 0 1 2
}

console.log(a, b, c) // => err