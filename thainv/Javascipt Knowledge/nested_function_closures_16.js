//Nested function là các hàm lồng vào nhau kiểu như 1 hàm con được thực hiện bên trong 1
// hàm lớn
//vd:
function sumSquare(a, b) {
	function square(x) {
		return x*x;
	}
	return square(a) + square(b);
}
console.log(sumSquare(2, 3));
//
function outside(x) {
  function inside(y) {
    return x + y;
  }
  return inside;
}
console.log(outside(3)(5));
// Ngoài ra ta còn có thể lồng nhiều function vào với nhau.

// Closures là một hàm có thể ghi nhớ nơi nó được tạo ra và truy cập được biến ở bên
// ngoài phạm vi của nó.
// Function to increment counter
function count() {
  let counter = 0;
  counter += 1;
  return counter;
}

// Call add() 3 times
add(); // => 1
add(); // => 1
add(); // => 1

// Đầu tiên ta khai báo hàm count, tiếp theo t gán hàm add bằng với hàm count() thì
// nó sẽ gọi hàm vàlúc này hàm add sẽ là cái return trả về nó sẽ là hàm increase. 
// Như vậy ta có thể đưa 1 biến cục bộ private ra bên ngoài để dùng và dữ liệu sẽ không
// bị mất đi. Cứ như vậy mỗi khi ta gọi hàm add thì nó sẽ thực thi hàm increase, lúc bây
// giờ hàm increase đã kh còn private nên counter sẽ không bị reset.
// Vì khi ta gán add = count() thì khi ta gọi add biến counter sẽ luôn nằm trong vùng
// nhớ ban đầu nên nó kh bị reset.
function count() {
  let counter = 0;
  function increase() {
  	return ++counter;
  }
  return increase;
};

const add = count();

console.log(add()); // => 1
console.log(add()); // => 2
console.log(add()); // => 3