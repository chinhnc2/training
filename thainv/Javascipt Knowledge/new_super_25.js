//super: là từ khóa dùng để truy cập và gọi hàm từ obj cha của nó
//vd:
class Rectangle {
  static logNbSides() {
    return 'I have 4 sides';
  }
}

class Square extends Rectangle {
  static logDescription() {
    return super.logNbSides() + ' which are all equal';
  }
}
Square.logDescription(); // 'I have 4 sides which are all equal'

//new: là từ khóa dùng để tạo 1 obj đã được xác định hoặc obj dựng sẵn có hàm khởi tạo
// constructor()
//vd:
function Car(make, model, year) {
  this.make = make;
  this.model = model;
  this.year = year;
}

const car1 = new Car('Eagle', 'Talon TSi', 1993);

console.log(car1.make);
// expected output: "Eagle"