// Convert String to Number và Type coercion (ép kiểu):
// parseInt(String) or parseInt(String, radix) với radix là hệ cơ số như 6, 8, 16, 10, ...
// convert string to int;
console.log(parseInt(105)) //=>105
console.log(parseInt(105.8)) //=>105 làm tròn xuống
console.log(parseInt('105')) // => 105
console.log(parse('F', 16)) // => 15
console.log(parseInt('aaaa')); //=>NAN vì string này kh thể convert sang number được.
// parseFloat
console.log(parseFloat(3.14));
console.log(parseFloat('3.14'));
console.log(parseFloat('  3.14  '));
console.log(parseFloat('314e-2'));
console.log(parseFloat('0.0314E+2'));
console.log(parseFloat('3.14some non-digit characters'));
// đều return 3.14

//Conver by number() + ép kiểu
const value1 = '5';
const value2 = 9;
let sum = value1 + value2;
console.log(sum) //=> '59' ép kiểu sang string
sum = Number(value1) + value2;

console.log(sum); // 59: ép kiểu sang number.

//Unary Operator:
var a = '10';
console.log(+a); //=>10

//Math.floor('123')
//console.log('14'*1);