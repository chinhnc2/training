// Callbacks là 1 hàm A được truyền vào 1 hàm khác B như 1 tham số và được B sử 
// dụng trong đó.
// vd:
function doSomething() {
	alert('vào');
}
function something(doCallback) {
	doCallback();
}
something(doSomething);

// Lưu ý khi truyền 1 tham số là hàm vào 1 hàm khác thì ta không nên để () vì
// nếu như vậy nó sẽ gọi hàm luôn thay vì chỉ xem hàm như 1 tham số.
// Ngoài ra callbacks còn có thể giúp asynchronous xử lý bất đồng bộ, đợi hàm A
// thực thi xong thì mới làm tiếp các hàm khác.
// vd:
p_client.open(function(err, p_client) {
   p_client.dropDatabase(function(err, done) {
      //......
   });
});

