// Unary Operator
//delete: dùng để xóa phần tử trong obj
//vd:
delete Math.PI; // returns false (cannot delete non-configurable properties)

const myObj = {h: 4, k: 5};

delete myObj.h; // returns true (can delete user-defined properties)

console.log(myObj); // => {k : 5}

//void: để biểu thị 1 hàm không có giá trị trả về.
//vd:
void function test() {
  console.log('boo!');
  // expected output: "boo!"
}();

//typeOf: để trả về kiểu dữ liệu 
//vd:
var myFun = new Function('5 + 2');
var shape = 'round';
var size = 1;
var foo = ['Apple', 'Mango', 'Orange'];
var today = new Date();

typeof myFun;       // returns "function"
typeof shape;       // returns "string"
typeof size;        // returns "number"
typeof foo;         // returns "object"
typeof today;       // returns "object"
typeof doesntExist; // returns "undefined"
