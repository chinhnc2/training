// Function Parameters là các biến được truyền vào 1 hàm và được nhận trong function 
// function do(a, b) với a, b là các parameter nếu như hàm gọi function này không truyền 
// vào tham số thì nó sẽ là undefined.
 
function multiply(a, b) {
  console.log(b);
  consosle.log(a*b) // => NaN
  b = typeof b !== 'undefined' ?  b : 1;
  return a * b;
}

console.log(multiply(5)); // 5

// Ngoài ra ta còn có thể set default cho parameters tránh trường hợp gọi hàm nhưng
// thiếu truyền tham số sẽ dẫn đến undefined;
function multiply(a, b = 1) {
	  return a * b;
}

console.log(multiply(5)); // 5