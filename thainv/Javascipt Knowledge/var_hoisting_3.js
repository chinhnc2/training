/*Hoisting là hành động mặc định của Javascript, nó sẽ chuyển phần khai báo lên phía
 trên top Trong Javascript, một biến (variable) có thể được khai báo sau khi được sử 
 dụng*/

 //vd:
 console.log(name); //=> undefined
 var name = 200;

//Ta có: Chương trình sẽ tiến hành đưa biến name lên đầu tiên để khai báo tiếp đó nó sẽ
//thực hiện đồng bộ như sau:

var name;
console.log(name);
name = 200;

//Ở dòng 11 name được khai báo những chưa gán giá trị nên dòng 12 log ra undefined