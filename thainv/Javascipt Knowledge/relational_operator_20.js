//In: Dùng để kiểm tra xem 1 phần tử có thuộc 1 array hay obj hay gì đó, ... hay không
// và trả về kiểu boolean (true or false)
// Arrays
var trees = ['redwood', 'bay', 'cedar', 'oak', 'maple'];
0 in trees;        // returns true
3 in trees;        // returns true
6 in trees;        // returns false
'bay' in trees;    // returns false (you must specify the index number,
                   // not the value at that index)
'length' in trees; // returns true (length is an Array property)

// built-in objects
'PI' in Math;          // returns true
var myString = new String('coral');
'length' in myString;  // returns true

// Custom objects
var mycar = { make: 'Honda', model: 'Accord', year: 1998 };
'make' in mycar;  // returns true
'model' in mycar; // returns true

//typeOf: Dùng để kiểm tra kiểu dữ liệu của biến, trả về kiểu dữ liệu như function,
// obj, string, number, boolean, undefined, null, ...
var myFun = new Function('5 + 2');
var shape = 'round';
var size = 1;
var foo = ['Apple', 'Mango', 'Orange'];
var today = new Date();
console.log(typeof myFun);       // returns "function"
console.log(typeof shape);       // returns "string"
console.log(typeof size);        // returns "number"
console.log(typeof foo);         // returns "object"
console.log(typeof today);       // returns "object"
console.log(typeof doesntExist); // returns "undefined"

//Instance of: để kiểm tra 1 biến(instance) có thuộc 1 class nào đó không
var theDay = new Date(1995, 12, 17);
console.log(theDay instanceof Date); // => true