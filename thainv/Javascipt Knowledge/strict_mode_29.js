//Strict mode: là 1 từ khóa để chị định rằng đoạn code này ở trong "chế độ nghiêm ngặt"

// Để đưa code vào chế độ nghiệm ngặt ta cần khai báo nó:
"use strict";
// Nếu như ta muốn toàn bộ phần code ở trong chế độ nghiệm ngặt thì ta đặt nó ở đâu trang
// nếu không thì khi ta đặt ở đâu, những dòng code bên dưới sẽ phải tuân theo Strict
// mode
//vd:

"use strict";
x = 3.14; // => err: x is not defined vì strict mode không cho phép khai báo biến không keyword "var, let, const"

// Khi ta xóa 1 biến hay obj thì sẽ không được chấp nhận trong Strict mode
"use strict";
let x = 3.14;
delete x;   //err

//Nếu ta khai báo hàm với 2 tham số bị trùng thì cũng sẽ bị lỗi trong strict mode
"use strict";
function x(p1, p1) {}; //err

//Ta không thể khai báo biến trùng với các keyword trong JS
"use strict";
let arguments = 3.14; //err