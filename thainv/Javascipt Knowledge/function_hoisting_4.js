//Hoisting nó cho phép sử dụng 1 hàm trước khi hàm đó được khai báo
catName("Tiger");

function catName(name) {
  console.log("My cat's name is " + name);
}
/*
The result of the code above is: "My cat's name is Tiger"
*/

//thứ tự sẽ là nó sẽ kéo function lên và khai báo trước sau đó mới chạy hàm.