// BOM: Browser Object Model

// - Window:
// Window obj được hỗ trợ bởi tất cả các trình duyệt, nó cho biết cửa sổ của trình duyệt 
// hiện tại.
// Tất cả các biến global hay funcion đều là các thuộc tính của Window obj, ngoài ra các thẻ
// trong HTML dom cũng là các thuộc tính của window obj vì vậy ta có thể dễ dàng lấy ra
// Ngoài ra window còn có 1 vài lệnh cơ bản như:
// window.innerWidth -> chiều rộng của trang web hiện tại (trừ đi các tabar, ...);
// window.innerHeight -> chiều coa của trang web hiện tại ;
/*window.open() - open a new window
window.close() - close the current window
window.moveTo() - move the current window
window.resizeTo() - resize the current window*/

// - Location:
// window.location là obj có thể được sử dụng để lấy đường dẫn của cửa sổ trình duyêt
// hiện tại và có thể điều hướng 1 số thứ, ...
/*
window.location.href returns the href (URL) of the current page
window.location.hostname returns the domain name of the web host
window.location.pathname returns the path and filename of the current page
window.location.protocol returns the web protocol used (http: or https:)
window.location.assign() loads a new document
*/

// - History:
// window.history là 1 obj có thể truy cập vào lịch sử trình duyệt để thực hiện các
// tác vụ như lùi trang hay tiến tới 1 trang (có trong lịch sử).
// window.history.back()
// window.history.forward()

// - Popup box: nó sẽ show ra các cửa sổ bật lên nhỏ như alert, confirm, prompt.
// *Alert: 1 cửa sổ nhỏ được bật lên, chủ yếu dùng để thông báo 1 điều gì đó đến
// users.
// syntax: window.alert(string) or alert(string)
// *Confirm: 1 sửa sổ nhỏ được bật lên, dùng để xác thực 1 điều gì đó trước khi thực hiện
// syntax: window.confirm(string) or confirm(string) với string là 1 yêu cầu hay câu
// hỏi gì đó cần được xác thực.
// *Prompt: tương tự như alert và confirm nhưng được dùng để nhập 1 thứ gì đó vào trước
// khi thực hiện 1 việc gì đó.
// syntax: prompt(string, default string) or prompt(string)

// - Cookies: là nơi để lưu trữ các thông tin cá nhân hay các dữ liệu nhỏ trong trang web
// syntax: 
// create: document.cookie = "username=John Doe";
// read: let x = document.cookie;

// - Screen: Chứa thông tin về màn hình của khách hàng.
/*
	screen.width: cho biết kích thướh chiều rộng màn hình của user
	screen.height: cho biết kích thước chiều cao màn hình của user
	screen.colorDepth: trả về số bit đưuọc sử dụng để show ra 1 màu.
	screen.pixelDepth: trả về độ sâu pixel của màn hình.
*/