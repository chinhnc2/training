// Có 3 kiểu scope là Global scope, function scope và block scope
// biến Global là biến có thể dùng được ở bất cứ đâu
// biến function scope là biến được khai báo trong function và chỉ được dùng trong đó.
// biến block scope là biến được khai báo trong block {} và chỉ được dùng trong đó.
// Ngoài ra biến var còn có thể là Global variable nếu nó kh đặt trong function scope.
// Khi ta khai báo 1 biến mà không dùng keyword thì tự hiểu nó sẽ là global variable.

//vd
var a = 1;
let b = 2;
const c = 3;

function show() {
	console.log(a); //=> 1
	console.log(b); //=>2
	console.log(c);	//=>3

}

function show1() {
	var d = 1;
	let e = 2;
	const f = 3;
}
console.log(d) //=>err
console.log(e) //=>err
console.log(f) //=>err

var n = 0;
if (n === 0) {
	var x = 1;
	let y = 2;
	const z = 3;
}
console.log(x); // 1
console.log(y); //=> err
console.log(z); //=>err
