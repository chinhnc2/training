var fs = require('fs');

const getDaysInMonth = (month) => {
  return new Date(2022, month, 0).getDate();
}

const getDataJson = (month) => {
  let now = new Date(Date.now());
  let monthCurrent = now.getMonth();
  //console.log(monthCurrent);
  if (month > monthCurrent+1) return [];
  let data = fs.readFileSync('data1.json', {encoding: 'utf8', flag: 'r'});
  //console.log(data);
  data = JSON.parse(data);
  return data.find(x => Object.keys(x) == month)[month];
}

const renderCheckInfor = (month, data) => {
  // Đọc data từ file json.
  //let data = fs.readFileSync('data.json', {encoding: 'utf8', flag: 'r'});
  //console.log(data);
  let daysInMonth = getDaysInMonth(month);
  let checkInfor = [];
  let now = new Date(Date.now());
  let dayCurrent = now.getDate();
  let monthCurrent = now.getMonth();
    
  --month;
  for (let i = 1; i <= daysInMonth; i++) {
    //console.log(data[0].time);
    console.log(new Date(2022, month, i).getTime());
    // Kiểm tra xem nếu khách hàng muốn xem thời gian từ hiện tại -> quá khứ
    let tick = data.length > 0 && data.find(x => new Date(x.time).getTime() == new Date(2022, month, i).getTime().toString());
    //console.log(tick);
    tick ? checkInfor.push(tick) : checkInfor.push({time: new Date(2022, month, i), checkIn: null, checkOut: null});
  }
  //console.log(checkInfor)
  let obj = {};
  obj[month + 1] = checkInfor;
  if (data.length === 0) {
    fs.writeFileSync('data.json', JSON.stringify([obj])) // Ghi data vào file json
  } else fs.writeFileSync('data.json', JSON.stringify([...data, obj]))
  
  return checkInfor;
}

// Chuyển đổi data từ Int Time sang Date Time.
const convertToDateTime = (resultInt) => {
  let resultDate = [...resultInt];
  resultDate = resultDate.map((x) => {
  //console.log(x)
  if (x.checkIn > 0 && x.checkOut > 0) {
    return {...x, checkIn: new Date(x.checkIn), checkOut: new Date(x.checkOut)}
  } else {
    return {... x};
  }
  })
  return resultDate;
}

//Đếm ra số ngày đã chấm công
const getDayOnOffice = () => {
  let count = resultIntTime.filter(x => x.checkIn > 0 && x.checkOut > 0).length;
  return count;
}

//Kiểm tra chấm công trong năm 2022;
let m = 2; // Tháng

// Lấy ra 1 mảng bảng chấm công trong tháng m với thời gian là int
let resultIntTime = renderCheckInfor(m, getDataJson(m));
console.log(resultIntTime);

// Convert mảng bảng chấm công trong tháng m từ int sang Date
let resultDateTime = convertToDateTime(resultIntTime);
//console.log(resultDateTime);



// Đếm những ngày đi làm
let getDayOn = getDayOnOffice();
console.log(getDayOn);