const pay = (n) => {
    if (n === 0) return 0;
    return n + pay(n-1);
}
console.log(pay(10));