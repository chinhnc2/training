import logo from './logo.svg';
import './App.css';
import './style.scss';
import { useState, useEffect } from 'react';
import axios from 'axios';



function App() {

  const listType = [
    "" ,
              'frozen',
              'wooden',
              'cotton',
              'fresh',
              'metal',
  ]

  const listStore = [
  "",
            'magenta',
            'black',
            'white',
            'blue',
            'green',
            "yellow" ,
            "pink" ,
            "turquoise" ,
  ];

  const [data, setData] = useState([]);
  const [products, setProducts] = useState([]);
  const [keyQuery, setKeyQuery] = useState('');
  const [keyType, setKeyType] = useState('');
  const [keyStore, setKeyStore] = useState('');
  const [sortKey, setSortKey] = useState(0);
  const [carts, setCarts] = useState([]);
  const [storeIDs, setStoreIDs] = useState([]);
  const [lenCart, setLenCart] = useState(0);
  const [sumPrice, setSumPrice] = useState(0);

  const onSearchItem = (e) => {
    setKeyQuery(e.target.value);
    setProducts(searchItem(e.target.value));
  }
  const searchItem = (val) => {
    if (val.length > 0) {
      let searchList = products.filter(x => x.name.toLowerCase().indexOf(val.toLowerCase()) >= 0);
      return searchList;
    }
    setKeyType('');
    return data;
  }
  const onSort = (e) => {
    if (e.target.value !== sortKey) { 
      setSortKey(e.target.value);
      setProducts(sortByPrice(e.target.value));
    } else {
      setSortKey(0);
      setProducts(data);
    }
  }
  const sortByPrice = (n) => { // Sắp xếp các sản phẩm theo giá.
    let items;
    if ( n > 0) { // up
      items = [...products]
      items = items.sort((a, b) => a.price - b.price);
    } else if (n < 0) { // down
      items = [...products];
      items = items.sort((a, b) => b.price - a.price);
    } else {
      setKeyQuery('');
      setKeyType('');
      items = data;
    }
    return items;
  }
  const onChangeSelectType = (e) => {
    setKeyType(e.target.value);
    setProducts(groupByType(e.target.value));
  }
  const groupByType = (val) => {
    if (val.length > 0) {
      let groupList = products.filter(x => x.type.toLowerCase() === val);
      return groupList;
    }
    setKeyQuery('');
    return data;
  }
  const onChangeSelectStore = (e) => {
    setKeyStore(e.target.value);
    setProducts(groupByStore(e.target.value));
  }
  const groupByStore = (val) => { // Lọc các sản phẩm theo StoreID; 
    if (val.length > 0) {
      let groupList = products.filter(x => x.storeID.toLowerCase() === val);
      return groupList;
    }
    setKeyQuery(''); // Reset từ khóa tìm kiếm nếu bỏ chọn lọc theo Group
    return data;
  }
  const addStoreIDs = (x) => {
    if (storeIDs.indexOf(x) < 0) {
      let temp = [...storeIDs, x];
      console.log(temp);
      setStoreIDs(temp);
    }
  }
  const addToCart = (item) => { // Thêm 1 sản phẩm vào giỏ hàng
    let index = carts.findIndex(x => x._id == item._id);
    let items;
    if (index >= 0) { // Nếu có item trong giỏ hàng thì sẽ tăng nó thêm 1 đơn vị số lượng
      items = [...carts.slice(0, index), {...item, count: carts[index].count+1}, ...carts.slice(index+1)];
      setCarts(items);
    } else { // ngược lại thì sẽ thêm nó vào với số lượng là 1.
      console.log(item);
      items = [...carts, {...item, count: 1}];
      setCarts(items);
    }
    addStoreIDs(item.storeID);
  } 
 

  const increaseItem = (item) => { // Tăng thêm 1 đơn vị cart
    let index = carts.findIndex(x => x._id == item._id);
    let items = [...carts.slice(0, index), {...item, count: ++item.count}, ...carts.slice(index+1)];
    setCarts(items);
  }
  const decreaseItem = (item) => { // Giảm đi 1 đơn vị cart
    let index = carts.findIndex(x => x._id == item._id);
    let items
    if (item.count > 1) {
       items = [...carts.slice(0, index), {...item, count: --item.count}, ...carts.slice(index+1)];
    } else {   
      items = [...carts.slice(0, index), ...carts.slice(index+1)];
    }
    setCarts(items);
  }
  const removeItem = (item) => { // xóa item được chọn trong giỏ hàng
    let index = carts.findIndex(x => x._id == item._id);
    let items = [...carts.slice(0, index), ...carts.slice(index+1)];
    setCarts(items);
  }
  const totalCart = () => { // Tính tổng số lượng hàng trong giỏ
    let cartLength = 0;
    carts.forEach(x => cartLength += x.count);
    setLenCart(cartLength);
  }
  const totalPrice = () => { // Tính tổng giá của tất cả các hàng có trong giỏ
    let sum = 0;
    carts.forEach(x => sum += x.count * x.price);
    setSumPrice(sum);
  }

  const clearCart = () => {
    setCarts([]);
  }



  useEffect(() => { // Mỗi khi render trang lần đầu thì sẽ gọi api đến trang trên và lấy data về.
    //let res;
    //https://6215b8eec9c6ebd3ce2fc8c1.mockapi.io/luta/products
    axios.get('https://6215b8eec9c6ebd3ce2fc8c1.mockapi.io/luta/products').then(res => {
      console.log(res);
      if (res.status == 200) {
        console.log(res.data);
        //console.log(res);
        setData(res.data);
        setProducts(res.data);
      } else {
        alert(res.status + res.msg);
      }
    } ).catch((err) => alert(err));
    
  }, []);

  useEffect(() => { // Chạy hàm tính số lượng Cart và tổng giá của chúng để cập nhật lại State
    totalCart();
    totalPrice();
  }, [carts])
  
  //Sắp xếp giỏ hàng theo StoreID (1 div border với title là tên Store bên trong là 1 list các sản phẩm trong giỏ hàng thuộc Store đó)
  return (
    <div className="App">
      <div className='nav_bar d_flex_hor'>
        <div className='menu_icon d_none d_block_mb text_weight'>+</div>
        <div className='logo text_weight'>Luta</div>
        <div className='header_top d_flex_hor'>
        <div className='nav_bar_item'>
          <p>Categories</p>
        </div>
        <div className='nav_bar_search_item'>
          <input type='text' pla  ceholder='Search for anything' />
        </div>
        <div className='nav_bar_item'>
          <p>Business</p>
        </div>
        <div className='nav_bar_item'>
          <p>History</p>
        </div>
        </div>
        <div className='d_none d_block_mb btn_bar '><button className='text_blur' onClick={() => {}}>Search</button></div>
        <div className='nav_bar_item d_flex_hor'>
          <div className='nav_bar_item_child'>
            <p>{lenCart}</p>
          </div>
          <div className='nav_bar_item_child btn_bar d_none_mb'>
            <button onClick={() => {}} >Login</button>
          </div>
          <div className='nav_bar_item_child btn_bar btn_signup d_none_mb'>
            <button onClick={() => {}} >SignUp</button>
          </div>
        </div>

      </div>
      <div className='body_screen'>
      <div className='title_screen d_none_mb'><p>Shopping Cart</p></div>
      <div className='item_carts d_flex_hor d_flex_ver_mb'>
        <div className='d_flex_ver item_carts_child order_2_mb'>
          {storeIDs.length > 0 && storeIDs.map((element) => (
          <div className='d_flex_ver bd'>
            <div className='text_blur'><p>{element}</p></div>
        {carts.length > 0  && carts.map((x) => element == x.storeID && (<div className='cart d_flex_hor bd f_wrap'>
          <div className='img_cart'><img src={x.avatar} alt='image_cart' /></div>
          <div className='d_flex_hor f_wrap'>
            <div className='d_flex_ver des_cart'>
              <p className='fw_bold'>{x.name}</p>
              <p className='des_cart_child'>{x.des}</p>
            </div>
            <p>{x.count}</p>
            <div className='handle_cart d_flex_ver'>
              <button onClick={() => increaseItem(x)} >+</button>
              <button onClick={() => decreaseItem(x)} >-</button>
              <button onClick={() => removeItem(x)} >x</button>
            </div>
            <p className='color_purple'>{x.price} $</p>
          </div>
          
        </div>))}
        </div>
        ))}
        </div>
        <div className='d_flex_ver about_cart order_1_cart'>
          
          <p>Giỏ hàng: <span className='fw_bold'>{lenCart}</span></p>
          <p className='color_red'>Tổng giá: <span className='text_weight'>{sumPrice} $</span> </p>
          <button>Thanh toán</button>
          <button onClick={clearCart}>Clear cart</button>
        </div>
      </div>
      
      <div className='tool_bar bd d_flex_hor d_flex_ver_mb'>
        <div className='search_item'>
          <input type='text' placeholder='search here' value={keyQuery} onChange={onSearchItem} />
        </div>
        <div className='group_item'>
            <input type="text" list="listType" onChange={onChangeSelectType} placeholder='select type' value={keyType} />
            <datalist id="listType" >
              {listType.map(type => 
              <option value={type}/> )}
            </datalist>
        </div>
        <div className='group_item'>
            <input type="text" list="listStore" onChange={onChangeSelectStore} placeholder='Select Store' value={keyStore} />
            <datalist id="listStore" >
              {listStore.map(store => 
              <option value={store} /> )}
            </datalist>
        </div>
        <div className='sort_item'>
          <label>Tăng dần
            <input type='checkbox' checked={sortKey > 0 ? true: false} value={1} onChange={onSort} />
          </label>
          <label>Gỉam dần
            <input type='checkbox' checked={sortKey < 0 ? true : false} value={-1} onChange={onSort} />
          </label>
        </div>
      </div>
      <div className='products d_grid d_grid_medium d_grid_small  '>
      {products.length > 0 && products.map(x => (<div className='item'>
        <div className='img_item'>
          <img src={x.avatar} alt='image item' />
        </div>
        <div className='name_item'>
          <p className='text_weight'>{x.name}</p>
        </div>
        <div className='des_item'>
          <p className='text_blur'>{x.des}</p>
        </div>
        <div className='footer_item d_flex_hor  '>
          <div className='price_item'>
            <p>{x.price} $</p>
          </div>
          <div className='to_cart_item'>
            <button onClick={() => addToCart(x)} >Add</button>
          </div>
        </div>
      </div>))}
      </div>
      </div>
    </div>
  );
}

export default App;
