import React from 'react';
import '../App.css';
import '../style.scss';

const Order = ({ lenCart, sumPrice, clearCart }) => {
    return(
            <div className='Order d_flex_ver about_cart order_1_cart'>
                <p>Giỏ hàng: <span className='fw_bold'>{lenCart}</span></p>
                <p className='color_red'>Tổng giá: <span className='text_weight'>{sumPrice} $</span> </p>
                <button>Thanh toán</button>
                <button onClick={clearCart}>Clear cart</button>
            </div>
    )
}
export default Order;