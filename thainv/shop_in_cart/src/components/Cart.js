import React from 'react';
import '../App.css';
import '../style.scss';

const Cart = ({ x, increaseItem, decreaseItem, removeItem }) => {
    return(
        <div className='Cart'>
            <div className='cart d_flex_hor bd f_wrap'>
                <div className='img_cart'><img src={x.avatar} alt='image_cart' /></div>
                <div className='d_flex_hor f_wrap'>
                    <div className='d_flex_ver des_cart'>
                        <p className='fw_bold'>{x.name}</p>
                        <p className='des_cart_child'>{x.des}</p>
                    </div>
                    <p>{x.count}</p>
                    <div className='handle_cart d_flex_ver'>
                        <button onClick={() => increaseItem(x)} >+</button>
                        <button onClick={() => decreaseItem(x)} >-</button>
                        <button onClick={() => removeItem(x)} >x</button>
                    </div>
                    <p className='color_purple'>{x.price} $</p>
                </div>    
            </div>
        </div>
    )
}
export default Cart;