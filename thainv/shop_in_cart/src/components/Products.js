import React from 'react';
import '../App.css';
import '../style.scss';

const Product = ({ x, addToCart }) => {
    return(
        <div className='Product'>
            <div className='item'>
                <div className='img_item'>
                <img src={x.avatar} alt='image item' />
                </div>
                <div className='name_item'>
                <p className='text_weight'>{x.name}</p>
                </div>
                <div className='des_item'>
                <p className='text_blur'>{x.des}</p>
                </div>
                <div className='footer_item d_flex_hor  '>
                <div className='price_item'>
                    <p>{x.price} $</p>
                </div>
                <div className='to_cart_item'>
                    <button onClick={() => addToCart(x)} >Add</button>
                </div>
                </div>
            </div>
        </div>
    )
}
export default Product;