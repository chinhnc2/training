import React from 'react';

const ToolBar = ({ keyQuery, keyStore, keyType, sortKey, onSearchItem, onChangeSelectStore, onChangeSelectType, onSort}) => {
    const listType = [
        "" ,
                  'frozen',
                  'wooden',
                  'cotton',
                  'fresh',
                  'metal',
      ]
    
      const listStore = [
      "",
                'magenta',
                'black',
                'white',
                'blue',
                'green',
                "yellow" ,
                "pink" ,
                "turquoise" ,
      ];
    return(
        <div className='ToolBar'>
            <div className='tool_bar bd d_flex_hor d_flex_ver_mb'>
                <div className='search_item'>
                <input type='text' placeholder='search here' value={keyQuery} onChange={onSearchItem} />
                </div>
                <div className='group_item'>
                    <input type="text" list="listType" onChange={onChangeSelectType} placeholder='select type' value={keyType} />
                    <datalist id="listType" >
                    {listType.map(type => 
                    <option value={type}/> )}
                    </datalist>
                </div>
                <div className='group_item'>
                    <input type="text" list="listStore" onChange={onChangeSelectStore} placeholder='Select Store' value={keyStore} />
                    <datalist id="listStore" >
                    {listStore.map(store => 
                    <option value={store} /> )}
                    </datalist>
                </div>
                <div className='sort_item'>
                <label>Tăng dần
                    <input type='checkbox' checked={sortKey > 0 ? true: false} value={1} onChange={onSort} />
                </label>
                <label>Gỉam dần
                    <input type='checkbox' checked={sortKey < 0 ? true : false} value={-1} onChange={onSort} />
                </label>
                </div>
            </div>
        </div>
    )
}
export default ToolBar;