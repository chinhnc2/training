import React from 'react';
import '../App.css';
import '../style.scss';

const TopBar = ({ lenCart }) => {
    return(
        <div className='TopBar'>
            <div className='nav_bar d_flex_hor'>
                <div className='menu_icon d_none d_block_mb text_weight'>+</div>
                <div className='logo text_weight'>Luta</div>
                <div className='header_top d_flex_hor'>
                <div className='nav_bar_item'>
                <p>Categories</p>
                </div>
                <div className='nav_bar_search_item'>
                <input type='text' pla  ceholder='Search for anything' />
                </div>
                <div className='nav_bar_item'>
                <p>Business</p>
                </div>
                <div className='nav_bar_item'>
                <p>History</p>
                </div>
                </div>
                <div className='d_none d_block_mb btn_bar '><button className='text_blur' onClick={() => {}}>Search</button></div>
                <div className='nav_bar_item d_flex_hor'>
                <div className='nav_bar_item_child'>
                    <p>{lenCart}</p>
                </div>
                <div className='nav_bar_item_child btn_bar d_none_mb'>
                    <button onClick={() => {}} >Login</button>
                </div>
                <div className='nav_bar_item_child btn_bar btn_signup d_none_mb'>
                    <button onClick={() => {}} >SignUp</button>
                </div>
                </div>

            </div>
        </div>
    )
}
export default TopBar;