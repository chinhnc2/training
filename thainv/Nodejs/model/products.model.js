const mongoose = require('mongoose');

//Tương tự 1 constructor của product.
const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    des: {
        type: String,
        required: true
    },
    avatar: {
        type: String
    },
    price: {
        type: Number,
        required: true
    },
    type: {
        type: String,
        default: 'Cotton'
    },
    storeID: {
        type: String,
        required: true
    }
})
const Product = mongoose.model('Product', productSchema, 'products');
module.exports = Product;