const express = require('express');
const getProductController = require('../controller/products.controller');

const router = express.Router();

router.route('/get').get(getProductController); //Khai báo đường dẫn /get với method là get và controller được truyền vào bên trong để xử lý req và res.

module.exports = router;