const { resolveSoa } = require('dns');
const getProductService = require('../service/products.service');
const catchAsync = require('../utils/catchAsync');

const getProduct = catchAsync(async (req, res, next) => {
    const products = await getProductService();
    //console.log(products);
    if (products) {
        //res.statusCode = 200;
        res.status(200).json(products);
    } else { // Nếu kh có products thì sẽ trả lỗi msg là 'err' với status 404.
        res.statusCode = 404;
        res.msg('err');
    }
});

module.exports = getProduct;