const express = require('express');
const routes = require('./route/products.route');
const cors = require('cors');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors()); // Sử dụng để tránh các lỗi liên quan đến cors.

app.use('/', routes); // Khai báo đường dẫn / với route bên trong là routes.

module.exports = app; // Xuất biến app để các nơi khác có thể import vào dùng.
