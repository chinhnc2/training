// CatchAsync sẽ thực hiện 1 Promise để bắt lỗi các callback bên trong.
const catchAsync = (fn) => (req, res, next) => { //Truyền vào tham số là 1 hàm fn và trong hàm fn sẽ có 1 hàm callBack là (req, res, next) =>
    Promise.resolve(fn(req, res, next)).catch((err) => next(err));
  };
  
  module.exports = catchAsync;