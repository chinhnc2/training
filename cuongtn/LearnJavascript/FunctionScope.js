// Function Scope
// JavaScript has function scope: Each function creates a new scope.

// Variables defined inside a function are not accessible (visible) from outside the function.

// Variables declared with var, let and const are quite similar when declared inside a function.

// They all have Function Scope:

function myFunction() {
    var carName = "Volvo";   // Function Scope
}
function myFunction1() {
    let carName = "Volvo";   // Function Scope
}
function myFunction2() {
    const carName = "Volvo";   // Function Scope
}


// The following variables are defined in the global scope
var num1 = 20,
num2 = 3,
name = 'Chamakh';

// This function is defined in the global scope
function multiply() {
return num1 * num2;
}

multiply(); // Returns 60

// A nested function example
function getScore() {
var num1 = 2,
  num2 = 3;

function add() {
return name + ' scored ' + (num1 + num2);
}

return add();
}

getScore(); // Returns "Chamakh scored 5