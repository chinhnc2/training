// Primary expressions - this

//A function's this keyword behaves a little differently in JavaScript compared to other languages.
// It also has some differences between strict mode and non-strict mode.

//In most cases, the value of this is determined by how a function is called (runtime binding).
// It can't be set by assignment during execution, and it may be different each time the function is called.

//EX
const test = {
    prop: 42,
    func: function() {
      return this.prop;
    },
  };
  
  console.log(test.func());
  // expected output: 42


//Global context : In the global execution context (outside of any function), this refers to the global object whether in strict mode or not.
// In web browsers, the window object is also the global object:
console.log(this === window); // true

a = 37;
console.log(window.a); // 37

this.b = "MDN";
console.log(window.b)  // "MDN"
console.log(b)         // "MDN"


//Function context :Inside a function, the value of this depends on how the function is called.
function f1() {
    return this;
  }
  
  // In a browser:
  f1() === window; // true
  
  // In Node:
  f1() === globalThis; // true

