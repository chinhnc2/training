//1. Defining functions

// A JavaScript function is defined with the function keyword, followed by a name, followed by parentheses ().
// Function names can contain letters, digits, underscores, and dollar signs (same rules as variables).
// The parentheses may include parameter names separated by commas:
// (parameter1, parameter2, ...)
// The code to be executed, by the function, is placed inside curly brackets: {}
function name(parameter1, parameter2, parameter3) {
    // code to be executed
  }
//EX: 
function myFunction(p1, p2) {
  return p1 * p2;
}
document.getElementById("demo").innerHTML = myFunction(4, 3);
// Ex: Function Return
let x = myFunction(4, 3);   // Function is called, return value will end up in x

function myFunction(a, b) {
  return a * b;             // Function returns the product of a and b
}

//2. IIFE
// Description: An IIFE (Immediately Invoked Function Expression) is a JavaScript function that runs as soon as it is defined. The name IIFE is promoted by Ben Alman in his blog.

(function () {
    /* ... */
  })();
  //
  (function(window, name){ //code here 
    'use strict';
    greeting = 'Hello'; 
    console.log(greeting + ' ' + name) 
  })(window, name);
  //Ở đoạn code này JS sẽ throw error vì bạn đang cố truy cập một biến gobal trong IIFE
  //
  for(var i = 0; i < 10; i++){
    (function(i){//IIFE tạo ra một scope khác cho từng i, nên giá trị của i là khác nhau
     setTimeout(function(){
      console.log(i);
     });
    })(i);//biến i ở đây được tạo riêng cho từng IIFE 
   }