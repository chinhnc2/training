// JSON stands for JavaScript Object Notation
// JSON is a lightweight data interchange format
// JSON is language independent *
// JSON is "self-describing" and easy to understand
// * The JSON syntax is derived from JavaScript object notation syntax, but the JSON format is text only. Code for reading and generating JSON data can be written in any programming language.

//JSON Example
// This JSON syntax defines an employees object: an array of 3 employee records (objects):

// {
//     "employees":[
//       {"firstName":"John", "lastName":"Doe"},
//       {"firstName":"Anna", "lastName":"Smith"},
//       {"firstName":"Peter", "lastName":"Jones"}
//     ]
// }


    // JSON Objects
    // JSON objects are written inside curly braces.
    
    // Just like in JavaScript, objects can contain multiple name/value pairs:
    

    // {"firstName":"John", "lastName":"Doe"}}

    // JSON Arrays
// JSON arrays are written inside square brackets.

// Just like in JavaScript, an array can contain objects:

// "employees":[
//   {"firstName":"John", "lastName":"Doe"},
//   {"firstName":"Anna", "lastName":"Smith"},
//   {"firstName":"Peter", "lastName":"Jones"}
// ]