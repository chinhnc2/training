// Block statement { }


//The block statement is often called compound statement in other languages. It allows you to use multiple statements
        // where JavaScript expects only one statement. Combining statements into blocks is a common practice in JavaScript.

// Ex 1 : Block scoping rules with var or function declaration in non-strict mode
var y = 1;
{
  var y = 2;
}
console.log(y); // logs 2

 //Ex 2.1: Block scoping rules with let, const or function declaration in strict mode

 let x = 1;
{
  let x = 2;
}
console.log(x); // logs 1

 //Ex 2.2: Block scoping rules with let, const or function declaration in strict mode

const c = 1;
{
  const c = 2;
}
console.log(c); // logs 1 and does not throw SyntaxError...