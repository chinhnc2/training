// Left Hand Side

// 1 . new
//new is a keyword that lets a user create an object based off of a user-designed model.
//The model must be created first as a function, like this:
function Game(score, winner, loser) {
    this.score = score;
    this.winner = winner;
    this.loser = loser;
}
//this.score refers to the property of the object being created,
// while score refers to the parameter being passed into the function. We can create a new Game object with the following:
let SBLII = new Game(41-33, "Philadelphia Eagles", "New England Patriots");

console.log(SBLII.loser); //"New England Patriots"

//2. super
//Sometimes the information inside of an object is so large that we store it inside another object within the first.
// This is called "nesting". Think of it like a news program:

// let news = {
//     story 1: "headline",
//     story 2: "important",
//     weather: {
//         tomorrow: "sunny",
//         wednesday: "rain"
//     },
//     sports: {
//         baseball: "Home run!"
//         football: "Touchdown!",
//         ...
//     },
//     function throwToCommercial(segment, time) {
//         console.log("We'll be back with " + segment + " in " + time + "minutes after this short break.");
//     }
//     ...
// }
// super.throwToCommercial('sports', 2);