import './App.css';
import React, { useState,useEffect } from 'react';
import products from './product.json'
import axios from 'axios'
function App() {


  const [cart, setCart] = useState([]);
  const [cartStore, setCartStore] = useState([])

  useEffect(()=>{

    // async function getData(){
    //     const  res = await axios.get('http://localhost:3000/listcart')
    //     return res
    // }

    // getData().then(res1=>{
    //   console.log(res1.data)
    // })
    // getData().catch(err=>console.log(err))


  },[])

  function renderProduct() {
    // console.log(products)
    return products.map(function (value, index) {
      // console.log(value.name)
      return (
        <>
          <div key={value.id} className="product__item">
            <img className="item__img" src={value.img} alt="Ảnh xinh" />
            <h2 className="item__name">{value.name}</h2>
            <h3 className="item__price">{value.price}</h3>
            <button value={value.id} className="Add" onClick={() => addCart(value)}>Add</button>
          </div>
        </>
      )
    })
  }
  function renderCart() {
      return cart.map(function(value,index){
        console.log(cart)
        return(
          <>  
          <tr key={index}>
                  <td>{value.id}</td>
                  <td>{value.name}</td>
                  <td><img src={value.img} alt=''/></td>
                  <td>{value.price}</td>
                  <td><button type="button" onClick={()=>addCart(value)}>+</button></td>
                  <td>{value.quanlity}</td>
                  <td><button type="button" onClick={()=>downCart(value)}>-</button></td>
                  <td><button type="button" onClick={()=>deleteCart(value)}>X</button></td>
            </tr>
          </>
        )
      })
    
  }
  const addCart = (prd) => {
    console.log(prd)
    const checkCart = cart.find(item => item.id === prd.id)
    if (checkCart) {
      console.log("da co")
      console.log(checkCart)
      setCart(
        cart.map((item) =>
          (item.id === prd.id ? { ...checkCart, quanlity: checkCart.quanlity + 1 } : item)
        )
      )
     
    }
    else {
      console.log("chua co")
      setCart(state => [...state, { ...prd, quanlity: 1 }])
    }
    console.log(cart)
    renderGroupCart(prd.store)
  }

  const downCart = (prd) =>{
    const checkCartDown = cart.find(item=> item.id === prd.id)
    if(checkCartDown.quanlity>1){
      setCart( cart.map((item,index)=> item.id === prd.id ? {...checkCartDown,quanlity:prd.quanlity-1}:item ))
    }
    else{
      setCart(cart.filter(item=> item.id != prd.id))
    }
  }
  const deleteCart = (prd) =>{
    const checkDeleteCart = cart.find(item=>item.id === prd.id)
    if(checkDeleteCart){
      setCart(cart.filter(item=> item.id != prd.id))
    }
  }

  const renderSumCart = () => {
    let sumQty = 0;
    let sumPrice=0;
    cart.map((value,index)=>{
      sumQty += value.quanlity;
      sumPrice += value.quanlity * value.price; 
    })
    return(
      <>
        <tr className='sum__cart'>
            <td></td>
            <td>Sum</td>
            <td></td>
            <td>{sumPrice}</td>
            <td></td>
            <td>{sumQty}</td>
            <td></td>
          </tr>
    </>
    )
  }

  function renderGroupCart (itemStore) {
    console.log(itemStore)
    if(cartStore.indexOf(itemStore) <0){
      let item = [...cartStore,itemStore]
      setCartStore(item)
    }
    console.log(cartStore)
  }
  const clearAll = (e) =>
  {
    if(cart.length>0){
      console.log("clearAll")
      setCart([])

    }
  }

  return (
    <div className="App">
      <div className="header">
        <ul>
          <li>home</li>
          <li>cart</li>
        </ul>
      </div>
      <div className="product__listItem">
        {renderProduct()}
        {/* <div className ="product__item">
              <img className="item__img" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcROI2p6Nwj0Y0jMU_RHpNpwLVgI9TVYzYSecg&usqp=CAU" />
              <h2 className="item__name">Áo thun</h2>
              <h3 className="item__price">100000 VND</h3>
              <button className="Add">Add</button>
          </div> */}
      </div>
      <div className="product__cart">
        <table >      
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Image</th>
              <th>Price</th>
              <th>Up</th>
              <th>Quantity</th>
              <th>Down</th>
              <th>Delete</th>
            </tr>      
            {renderCart()}  
            {renderSumCart()}
        </table>
        <div className='deleteAll'>
          <button onClick={clearAll}>Clear All</button>
        </div>
      </div>
      <h1 className='store__cart'>Store Cart</h1>
      <div className='product__groupstore'>
        {
          cartStore.map(function(store){
            return(
              <>
              <h2 className='store__name'>{store}</h2>
            
              <div className='store__listItem'>
                  {/* <div className='store__item'>
                      <h2 className='item__name'>Áo Thun Mỹ</h2>
                      <h2 className='item__qty'>Quality : 10</h2>
                      <h2 className='item__price'>$100</h2>
                  </div> */}
                  {
                cart.map(function(item){
                  if(item['store'] === store){
                    return(
                      <div className='store__item'>
                      <h2 className='item__name'>{item.name}</h2>
                      <h2 className='item__qty'>Quanlity : {item.quanlity}</h2>
                      <h2 className='item__price'>${item.price}</h2>
                    </div>
                    )
                  }
                })
              }
    
              </div>
            </>
            )
          })
       
        }
      </div>
    </div>
  );
}

export default App;
