import React, { useState, useEffect } from 'react'
import axios from "axios"
import "./App.css"
import "./Main.scss"
import "./Style.scss"

function App() {
  const [data, setData] = useState([]);
  const [products, setProducts] = useState([]);
/*   const [keyQuery, setKeyQuery] = useState('');
  const [keyType, setKeyType] = useState('');
  const [keyStore, setKeyStore] = useState('');
  const [sortKey, setSortKey] = useState(0); */
  const [carts, setCarts] = useState([]);
  const [storeIDs, setStoreIDs] = useState([]);
  const [lenCart, setLenCart] = useState(0);
  const [sumPrice, setSumPrice] = useState(0);

/*   const onSearchItem = (e) => {
    setKeyQuery(e.target.value);
    setProducts(searchItem(e.target.value));
  } */
/*   const searchItem = (val) => {
    if (val.length > 0) {
      let searchList = products.filter(x => x.name.toLowerCase().indexOf(val.toLowerCase()) >= 0);
      return searchList;
    }
    setKeyType('');
    return data;
  } */
/*   const onSort = (e) => {
    if (e.target.value !== sortKey) { 
      setSortKey(e.target.value);
      setProducts(sortByPrice(e.target.value));
    } else {
      setSortKey(0);
      setProducts(data);
    }
  } */
/*   const sortByPrice = (n) => { // Sắp xếp các sản phẩm theo giá.
    let items;
    if ( n > 0) { // up
      items = [...products]
      items = items.sort((a, b) => a.price - b.price);
    } else if (n < 0) { // down 
      items = [...products];
      items = items.sort((a, b) => b.price - a.price);
    } else {
      setKeyQuery('');
      setKeyType('');
      items = data;
    }
    return items;
  } */
/*   const onChangeSelectType = (e) => {
    setKeyType(e.target.value);
    setProducts(groupByType(e.target.value));
  } */
/*   const groupByType = (val) => {
    if (val.length > 0) {
      let groupList = products.filter(x => x.type.toLowerCase() === val);
      return groupList;
    }
    setKeyQuery('');
    return data;
  } */
/*   const onChangeSelectStore = (e) => {
    setKeyStore(e.target.value);
    setProducts(groupByStore(e.target.value));
  } */
/*   const groupByStore = (val) => { // Lọc các sản phẩm theo StoreID; 
    if (val.length > 0) {
      let groupList = products.filter(x => x.storeID.toLowerCase() === val);
      return groupList;
    }
    setKeyQuery(''); // Reset từ khóa tìm kiếm nếu bỏ chọn lọc theo Group
    return data;
  } */
  const addStoreIDs = (x) => {
    if (storeIDs.indexOf(x) < 0) {
      let temp = [...storeIDs, x];
      console.log(temp);
      setStoreIDs(temp);
    }
  }
  const addToCart = (item) => { // Thêm 1 sản phẩm vào giỏ hàng
    let index = carts.findIndex(x => x.id === item.id);
    let items;
    if (index >= 0) { // Nếu có item trong giỏ hàng thì sẽ tăng nó thêm 1 đơn vị số lượng
      items = [...carts.slice(0, index), {...item, count: carts[index].count+1}, ...carts.slice(index+1)];
      setCarts(items);
    } else { // ngược lại thì sẽ thêm nó vào với số lượng là 1.
      console.log(item);
      items = [...carts, {...item, count: 1}];
      setCarts(items);
    }
    addStoreIDs(item.storeID);
  } 

  const increaseItem = (item) => { // Tăng thêm 1 đơn vị cart
    let index = carts.findIndex(x => x.id === item.id);
    let items = [...carts.slice(0, index), {...item, count: ++item.count}, ...carts.slice(index+1)];
    setCarts(items);
  }

/*   const handleAdd = (item) => {
    const itemExist = carts.find(cart => cart.id === item.id)
    
    if (itemExist) {
      setItems(carts.map((cart) => 
        cart.id === item.id ? {...itemExist, quantity: itemExist.quantity + 1} : cart)
      );
    } else {
      setItems([...carts, {...item, quantity: 1}])
    }
  } */

  const decreaseItem = (item) => { // Giảm đi 1 đơn vị cart
    let index = carts.findIndex(x => x.id === item.id);
    let items
    if (item.count > 1) {
        items = [...carts.slice(0, index), {...item, count: --item.count}, ...carts.slice(index+1)];
    } else {   
      items = [...carts.slice(0, index), ...carts.slice(index+1)];
    }
    setCarts(items);
  }

  const removeItem = (item) => { // xóa item được chọn trong giỏ hàng
    let index = carts.findIndex(x => x.id === item.id);
    let items = [...carts.slice(0, index), ...carts.slice(index+1)];
    setCarts(items);
  }
  
  const totalCart = () => { // Tính tổng số lượng hàng trong giỏ
    let cartLength = 0;
    carts.forEach(x => cartLength += x.count);
    setLenCart(cartLength);
  }
  
  const totalPrice = () => { // Tính tổng giá của tất cả các hàng có trong giỏ
    let sum = 0;
    carts.forEach(x => sum += x.count * x.price);
    setSumPrice(sum);
  }

  const clearCart = () => {
    setCarts([]);
  }

  useEffect(() => {
    const getPokemon = async() => {
      await axios.get("https://6215b8eec9c6ebd3ce2fc8c1.mockapi.io/luta/products")
      .then(res => {
        console.log(res);
        if (res.status === 200) {
          console.log(res.data);
          //console.log(res);
          setData(res.data);
          setProducts(res.data);
        } else {
          alert(res.status + res.msg);
        }
      } ).catch((err) => alert(err));
    };
    getPokemon();
  }, [])

  useEffect(() => { // Chạy hàm tính số lượng Cart và tổng giá của chúng để cập nhật lại State
    totalCart();
    totalPrice();
  }, [carts])
  
  //Sắp xếp giỏ hàng theo StoreID (1 div border với title là tên Store bên trong là 1 list các sản phẩm trong giỏ hàng thuộc Store đó)
  return (
    <div className="App">
      <div className='grid wide'>
        <div className=''><p>Shopping Cart</p></div>
          <div className='shop__name-cart'>
            <div className=''>
              {storeIDs.length > 0 && storeIDs.map((element) => (
              <div className=''>
                <div className='shop__name'>
                  <p>{element}</p>
                </div>
                {carts.length > 0  && carts.map((cart) => element === cart.storeID && (
                  <div className='form__cart'>
                    <div className='form__cart-img'>
                      <img src={cart.avatar} alt='' />
                    </div>
                    <div className=''>
                      <div className=''>
                        <p className=''>{cart.name}</p>
                        <p className=''>{cart.des}</p>
                      </div>
                      <p>{cart.count}</p>
                      <div className='handle_cart d_flex_ver'>
                        <button onClick={() => increaseItem(cart)} >+</button>
                        <button onClick={() => decreaseItem(cart)} >-</button>
                        <button onClick={() => removeItem(cart)} >x</button>
                      </div>
                      <p className='color_purple'>{cart.price} $</p>
                    </div>
                    
                  </div>
                ))}
              </div>
            ))}
            </div>
            <div className='price'>
              <p>Giỏ hàng: <span className='fw_bold'>{lenCart}</span></p>
              <p className='color_red'>Tổng giá: <span className='text_weight'>{sumPrice} $</span> </p>
              <button onClick={clearCart}>Clear cart</button>
            </div>
          </div>
          
          {/* <div className='tool_bar bd d_flex_hor d_flex_ver_mb'>
            <div className='search_item'>
              <input type='text' placeholder='search here' value={keyQuery} onChange={onSearchItem} />
            </div>
            <div className='group_item'>
                <input type="text" list="listType" onChange={onChangeSelectType} placeholder='select type' value={keyType} />
                <datalist id="listType" >
                  <option value="" />
                  <option value='frozen'/>
                  <option value='wooden'/>
                  <option value='cotton'/>
                  <option value='fresh'/>
                  <option value='metal'/>
                </datalist>
            </div>
            <div className='group_item'>
                <input type="text" list="listStore" onChange={onChangeSelectStore} placeholder='Select Store' value={keyStore} />
                <datalist id="listStore" >
                  <option value="" />
                  <option value='magenta'/>
                  <option value='black'/>
                  <option value='white'/>
                  <option value='blue'/>
                  <option value='green'/>
                  <option value="yellow" />
                  <option value="pink" />
                  <option value="turquoise" />
                </datalist>
            </div>
            <div className='sort_item'>
              <label>Tăng dần
                <input type='checkbox' checked={sortKey > 0 ? true: false} value={1} onChange={onSort} />
              </label>
              <label>Gỉam dần
                <input type='checkbox' checked={sortKey < 0 ? true : false} value={-1} onChange={onSort} />
              </label>
            </div>
          </div> */}
          <div className='grid wide'>
            <div className='row'>
              {products.length > 0 && products.map(x => (
                <div className='col l-2-4 m-4 c-12'>
                  <div className='item'>
                    <div className='item__img'>
                      <img src={x.avatar} alt='' />
                    </div>
                    <div className='item__desc'>
                      <p className='item__desc-name'>{x.name}</p>
                      <p className='item__desc-content'>{x.des}</p>
                      <div className=''>
                        <div className='price_item'>
                          <p>{x.price} $</p>
                        </div>
                        <div className='item__add'>
                          <button onClick={() => addToCart(x)} >Add</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
    </div>
  );
}
  
export default App;
  