7. Converting strings to numbers,Type coercion
- Type coercion:
+ Number to String Conversion: 
    example: var x = 10 + '10'      //(console.log(x) : 1010) string
             var x = '10' + 10      //(console.log(x) : 1010)
             var x = true + '10'    //(console.log(x) : true10)
+ String to Number Conversion:
    example: var x = 10 - '5'       //(console.log(x) : 5)  number
             var x = 10 / '5'       //(console.log(x) : 2)
             var x = 10 * '5'       //(console.log(x) : 50)
+ Boolean to Number:
    example: var x = true + 2       //(console.log(x) : 3)  number
             var x = false + 2      //(console.log(x) : 2)
+ The Equality Operator:
    example: var x = (10 == '10')       //(console.log(x) : true)  boolean
             var x = (true == 1)        //(console.log(x) : true)
             var x = (true == 'true')   //(console.log(x) : false)

8. Literals for Array, Object, Numeric, RegExp, String
    example1: (
        let name = "huy"
        let age = 20
        var sentence = `My name is ${name} and i am ${age}`  //(console.log(sentence) : My name is huy and i am 20) 
    ) 
    example2: (
        let course = ["JS", "CSS", "PHP"];
        let html = `
            <ul>
                <li>${course[0]}</li>
            </ul>
        `;
    ) 

9. Block statement { }
{
  StatementList
}
    example: (
        var x = 1;
        let y = 1;
        if (true) {
        var x = 2;
        let y = 2;
        }
        console.log(x);  // expected output: 2
        console.log(y);  // expected output: 1
    )

10. Exception handling statements (try...catch)
try {
  Block of code to try
}
catch(err) {
  Block of code to handle errors
}

11. Operators, spread operator
- Operators:
+ Arithmetic:  + , - , * , ** , / , % , ++ , -- 
+ Assignment:  = , -= , += , *= , /= , %= , **=
+ Comparison: == , === , != , !== , > , < , <= , >= , ?
+ Logical: && , || , !
+ Type: typeof, instanceof
+ Bitwise: & , | , ~ , ^ , << ,....
- Spread operator (...):
    example1: Math.max(1,3,5) // output: 5
             Math.max([1,3,5]) // output: NaN
             Math.max(...[1,3,5]) // output: 5
    example2: var course1 = [Javascript, HTML, CSS]
              var course2 = [PHP, Jquery]   
              var course3 = [...course1, ...course2] console.log(course3);  // expected output: [Javascript, HTML, CSS, PHP, Jquery]

12. Loops and iteration
    example1: for (let i = 0; i < 5; i++) {
                console.log('Huy dep trai'); (Runs 5 times, with values of step 0 through 4.)
              }
    example2: let i = 0;
              do {
                i += 1;
                console.log(i);
              } while (i < 5);
    example3: for (let i = 0; i < a.length; i++) {
                if (a[i] === theValue) {
                    break;
                }
              }

13. Defining functions, IIFE
- Defining functions:
    + Example1: function myFunction(x) {
                    return x * x 
               }
               myFunction(5) //25
    + Example2: const x = (x, y) => x * y;
                x(5, 10) //50
- IIFE: Immediately Invoked Function Expression
    + example1: ;(function () {
                    /* ... */
                })();
    + example2: ;(() => {
                    /* ... */
                })(); //Arrow function IIFE
    + example3; ;(async () => {
                    /* ... */
                })(); //async IIFE
      
14. Function scope
    example: function myFunction() {
                var courseName = "Javascript";   // Function Scope
            }
            function myFunction() {
                let courseName = "Javascript";   // Function Scope
            }
            function myFunction() {
                const courseName = "Javascript";   // Function Scope
            }
            console.log(carName)   //carName is not defined

15. Scope and the function stack
    example: function myFunction1() {
                myFunction2();
            }
            function myFunction2() {
                return "Hi!";
            }
            myFunction1();   //"Hi!"

16. Nested functions and closures
    example: function counter(){
                let count = 0
                function increase(){
                    return ++count
                }
                return increase()
            }
            console.log(counter())  //1
            console.log(counter())  //1
            console.log(counter())  //1
                    ==========
            function counter(){
                let count = 0
                function increase(){
                    return ++count
                }
                return increase
            }
            var test = counter()
            console.log(test())     //1
            console.log(test())     //2
            console.log(test())     //3

17. Arguments object
    example: function func1(a, b, c) {
                console.log(arguments[0]);  // expected output: 1
                console.log(arguments[1]);  // expected output: 2
                console.log(arguments[2]);  // expected output: 3
            }
            func1(1, 2, 3);

18. Function parameters (default, rest)
+ Default: function default(x = "default value"){
                console.log(x);   // expected output: default value
            }      
            default() 
+ Rest: var course = { 
            name: 'Reactjs'
            price: 1000, 
            time: "30days"
        }    
        var { name, ...newObject } = course;
        console.log(newObject); // expected output: {price: 1000, time: "30days"}

19. Arrow functions
- const test = (x) => {
    console.log(x); // expected output: 5 
}
test(5)
- const sum = (a, b) => a + b
console.log(sum(2, 3)); // expected output:5
- const sum = (a, b) => ({a: a, b: b}) (return object)

20. Relational operators (in, typeof, instanceof) 
- Comparison Operators: == , === , != , !== , > , < , <= , >= , ?
- in: propNameOrNumber in objectName
        var trees = ['redwood', 'bay', 'cedar', 'oak', 'maple'];
        0 in trees;        // returns true
        3 in trees;        // returns true
        6 in trees;        // returns false
        'bay' in trees;    // returns false (you must specify the index number,
                        // not the value at that index)
        'length' in trees; // returns true (length is an Array property)
- instanceof: objectName instanceof objectType
        var theDay = new Date(1995, 12, 17);
        if (theDay instanceof Date) {
            // statements to execute
        }
-typeof

21. Conditional (ternary) operator
var result = x > 5 ? "x lớn hơn 5" : "x bé hơn 5"

22. String operators,Comma operator
- String operator: 
+ console.log('my ' + 'string');
+ var mystring = 'alpha';
  mystring += 'bet';
- Comma operator:
var x = [0,1,2,3,4,5,6,7,8,9]
var a = [x, x, x, x, x];

for (var i = 0, j = 9; i <= j; i++, j--)
//                                ^
  console.log('a[' + i + '][' + j + ']= ' + a[i][j]);

23. Unary operators (delete, typeof, void)
- delete:   const Employee = {
                firstname: 'John',
                lastname: 'Doe'
            };
            console.log(Employee.firstname);    // expected output: "John"
            delete Employee.firstname;
            console.log(Employee.firstname);    // expected output: undefined
- void: 
+ void func()
+ void func()
+ example: <a href = "javascript:void(alert('Warning!!!'))">Click me!</a>    

24. Primary expressions - this
    + const iPhoneX = {
            // Property
        name: 'iPhoneX',
        color: "black",
        price: 10000
            // Method
        takePhoto() {
            console.log(this)
        }
    }
    console.log(iPhoneX.takePhoto);     //Trả về thằng cha mà gọi nó

25. Left-hand-side expressions(new, super)
-   new:
    function Car(name, price) {
        this.name = name;
        this.price = price;
    }
    const BMW = new Car(BMW, 2000);
    const mercerdi = new Car(mercer, 3000)
-   super:
    class Rectangle {
        static logNbSides() {
            return 'I have 4 sides';
        }
    }

    class Square extends Rectangle {
        static logDescription() {
            return super.logNbSides() + ' which are all equal';
        }
    }
    Square.logDescription(); // 'I have 4 sides which are all equal'

26. Regular expressions (RegExp)

