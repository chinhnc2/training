44. Array
- Array.prototype.concat()
- Array.prototype.copyWithin()
- Array.prototype.entries()
    + const a = ['a', 'b', 'c'];
        for (const [index, element] of a.entries())
        console.log(index, element);
        // 0 'a'
        // 1 'b'
        // 2 'c'
- Array.prototype.every()   //output: boolean
- Array.prototype.fill()
* Array.prototype.filter()   //output: Array
    + filter(function(element, index, array) { /* ... */ }, thisArg)
- Array.prototype.find()            \\ vi tri gan nhat
- Array.prototype.findIndex()       //
- Array.prototype.flat() 
    +   const arr1 = [0, 1, 2, [3, 4]];
        console.log(arr1.flat());   // expected output: [0, 1, 2, 3, 4]
        const arr2 = [0, 1, 2, [[[3, 4]]]];
        console.log(arr2.flat(2));  // expected output: [0, 1, 2, [3, 4]]
- Array.prototype.flatMap()
    +   var arr = [1, 2, 3, 4];
        arr.flatMap(x => [x, x * 2]);  // expected output: [1, 2, 2, 4, 3, 6, 4, 8]

- Array.prototype.forEach()
- Array.prototype.groupBy()
- Array.prototype.includes()  //output: boolean
- Array.prototype.indexOf()
- Array.prototype.join()
- Array.prototype.keys()
- Array.prototype.lastIndexOf()
* Array.prototype.map()
    + const array1 = [1, 4, 9, 16];
    const map1 = array1.map(x => x * 2)
    console.log(map1);      // expected output: Array [2, 8, 18, 32]
- Array.prototype.pop()     // 1 element each time call method
- Array.prototype.push()    // any elements 
* Array.prototype.reduce()
    + const array1 = [1, 2, 3, 4];
        const initialValue = 0;
        const sumWithInitial = array1.reduce(
        (previousValue, currentValue) => previousValue + currentValue
        ,initialValue);

        console.log(sumWithInitial);    // expected output: 10
    
    + reduce(function(previousValue, currentValue, currentIndex, array) { /* ... */ }, initialValue)

- Array.prototype.reduceRight()
    + const array1 = [[0, 1], [2, 3], [4, 5]].reduceRight(
        (accumulator, currentValue) => accumulator.concat(currentValue));

        console.log(array1);     // expected output: Array [4, 5, 2, 3, 0, 1]

- Array.prototype.reverse()
    + const array1 = ['one', 'two', 'three'];
        console.log('array1:', array1);         // expected output: "array1:" Array ["one", "two", "three"]
        const reversed = array1.reverse();
        console.log('reversed:', reversed);     // expected output: "reversed:" Array ["three", "two", "one"] 
        // Change the original array

- Array.prototype.shift()
- Array.prototype.unshift()
* Array.prototype.slice()
* Array.prototype.splice()
- Array.prototype.some()   // output: boolean, only 1 elemet right
* Array.prototype.sort()   // output: anphabet, number not from small to large 
- Array.prototype.toLocaleString()
    + toLocaleString(locales, options);
    + const array1 = [1, 'a', new Date('21 Dec 1997 14:12:00 UTC')];
        const localeString = array1.toLocaleString('en', { timeZone: 'UTC' });
        console.log(localeString);      // expected output: "1,a,12/21/1997, 2:12:00 PM",

- Array.prototype.toString()
    + const array1 = [1, 2, 'a', '1a'];
        console.log(array1.toString());     // expected output: "1,2,a,1a"

- Array.prototype.values()

45. Boolean
    + Creating Boolean objects with an initial value of false
        var bNoParam = new Boolean();
        var bZero = new Boolean(0);
        var bNull = new Boolean(null);
        var bEmptyString = new Boolean('');
        var bfalse = new Boolean(false);

    + Creating Boolean objects with an initial value of true
        var btrue = new Boolean(true);
        var btrueString = new Boolean('true');
        var bfalseString = new Boolean('false');
        var bSuLin = new Boolean('Su Lin');
        var bArrayProto = new Boolean([]);
        var bObjProto = new Boolean({});

46. Date
- Date.prototype.getDate()  //return day
- Date.prototype.getDay()   //retrun first day (Sunday - Saturday : 0 - 6)
- Date.prototype.getFullYear()
- Date.prototype.getHours()
- Date.prototype.getMilliseconds()
- Date.prototype.getMinutes()
- Date.prototype.getMonth()
- Date.prototype.getSeconds()
- Date.prototype.getTime()      //return milliseconds   
- Date.prototype.getTimezoneOffset()
- Date.prototype.toJSON()
    + const event = new Date('August 19, 1975 23:15:30 UTC');
        const jsonDate = event.toJSON();
        console.log(jsonDate);      // expected output: 1975-08-19T23:15:30.000Z
        console.log(new Date(jsonDate).toUTCString());  // expected output: Tue, 19 Aug 1975 23:15:30 GMT

- Date.prototype.toLocaleDateString()
    + const event = new Date(Date.UTC(2012, 11, 20, 3, 0, 0));
        const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
        console.log(event.toLocaleDateString('de-DE', options));    //expected output (varies according to local timezone):Donnerstag, 20. Dezember 2012
        console.log(event.toLocaleDateString('ar-EG', options));    // expected output (varies according to local timezone): الخميس، ٢٠ ديسمبر، ٢٠١٢

47. Object  
- Have 3 ways to create object
    +   const dog = {};                     // literal
        const cat = new Object();           // constructor
        const horse = Object.create({});    // static method

- Object.prototype.hasOwnProperty()

48. Strings
- Have 4 ways to create string
    +   const string2 = 'Also a string primitive';
        const string3 = `Yet another string primitive`;
        Copy to Clipboard
        const string4 = new String("A String object");
- String.prototype.charAt(index)
- String.prototype.charCodeAt(index)
- String.prototype.codePointAt()
    + const icons = '☃★♲';
      console.log(icons.codePointAt(1));  // expected output: "9733"
- String.prototype.concat()
- String.prototype.includes()
- String.prototype.endsWith()
- String.prototype.indexOf()
- String.prototype.lastIndexOf()
- String.prototype.match()
- String.prototype.matchAll()
- String.prototype.padStart(targetLength [, padString])
- String.prototype.repeat(count)
- String.prototype.replace()
- String.prototype.replaceAll(searchFor, replaceWith)
- String.prototype.search(regexp)
- String.prototype.slice(beginIndex[, endIndex])
- String.prototype.split([sep [, limit] ])
- String.prototype.startsWith(searchString [, length])
- String.prototype.substring()
- String.prototype.toLowerCase()
- String.prototype.toUpperCase()
- String.prototype.trim()
- String.prototype.valueOf()

49. Number
- Number.isNaN()
- Number.isFinite()
    + console.log(Number.isFinite(1 / 0));  // expected output: false
      console.log(Number.isFinite(10 / 5));   // expected output: true

- Number.isInteger()
- Number.parseFloat(string)
- Number.parseInt(string, [radix])
- Number.prototype.toExponential(fractionDigits)
- Number.prototype.toFixed()
    + function financial(x) {
        return Number.parseFloat(x).toFixed(2);
      }       
    console.log(financial(123.456));    // expected output: "123.46"
    console.log(financial(0.004));      // expected output: "0.00"
    console.log(financial('1.23e+5'));  // expected output: "123000.00"

- Number.prototype.toString([radix])

51. Math
- Math.PI
- Math.round()
    + Math.round(1.3)   // expected output: 1
    + Math.round(1.5)   // expected output: 2

- Math.abs()    //Tri tuyet doi
    + Math.abs(-4)      // expected output: 4
    + Math.abs(1.4)      // expected output: 1.4

- Math.ceil()   //Lam tron tren
    + Math.ceil(4.1)    // expected output: 5

- Math.floor()  //Lam trong duoi
    + Math.floor(4.99999) // expected output: 4

- Math.random() 
- Math.min()
- Math.max()

52. Falsy
- 6 values: 
    + 0
    + ''
    + null 
    + undefined
    + NaN
    + false