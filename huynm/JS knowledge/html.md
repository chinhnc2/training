1. Thẻ abbr trong HTML
<!-- Thẻ abbr sẽ định nghĩa các từ được viết tắt ví dụ PHP, JS, HTML,.. -->

2. Thẻ address trong HTML
<!-- Có chức năng xác định thông tin liên lạc của tác giả, chủ sở hữu bài viết -->

3. Thẻ audio trong HTML
<!-- Định nghĩa một file âm thanh -->

4. Thẻ b trong HTML
<!-- Nội dung văn bản in đậm -->

5. Thẻ blockquote trong HTML
<!-- Định nghĩa đoạn văn bản trích dẫn từ một nguồn khác -->

6. Thẻ body trong HTML
<!-- Xác định phần nội dung tài liệu -->

7. Thẻ canvas trong HTML
<!-- Sử dụng để vẽ đồ họa ngay trên trang thông qua JS -->

8. Thẻ caption trong HTML
<!-- Sử dụng để đặt tiêu đề cho bảng -->

9. Thẻ col trong HTML
<!-- Chỉ định thuộc tính cho mỗi cột trong một phần tử -->

10. Thẻ colgroup trong HTML
<!-- Xác định một nhóm hoặc nhiều cột trong bảng để phục vụ cho việc định dạng -->

11. Thẻ datalist trong HTML
<!-- Chỉ định một danh sách các tùy chọn được xác định trước cho một phần tử <input> -->

12. Thẻ del trong HTML
<!-- Xác định một đoạn văn bản sẽ bị xóa bỏ của một tài liệu -->

13. Thẻ details trong HTML
<!-- Xác định chi tiết bổ sung mà người dùng có thể ẩn hiện theo yêu cầu -->

14. Thẻ dialog trong HTML
<!-- Định nghĩa một hộp thoại hoặc một cửa sổ -->

15. Thẻ embed trong HTML
<!-- Xác định một vùng chứa cho một ứng dụng bên ngoài hoặc nội dung tương tác. -->

16. Thẻ figcaption trong HTML
<!-- Định nghĩa caption cho một phần tử figure. -->

17. Thẻ figure trong HTML
<!-- Chỉ định nội dung khép kín, như minh họa, sơ đồ, ảnh, danh sách mã, v.v ... -->

18. Thẻ i trong HTML
<!-- định nghĩa một đoạn văn bản được hiển thị với các chữ in nghiêng -->

19. Thẻ iframe trong HTML
<!-- chỉ định một nội dung định tuyến -->

20. Thẻ ins trong HTML
<!-- Xác định đoạn văn bản đc gạch dưới -->

21. Thẻ fieldset trong HTML
<!-- nhóm các phần tử có liên quan trong một form -->

22. Thẻ legend trong HTML
<!-- caption cho fieldset -->

23. Thẻ meter trong HTML
<!-- xác định một phép đo vô hướng trong phạm vi đã biết, hoặc biểu diễn một phân số dưới dạng một thước đo -->

24. Thẻ object trong HTML
<!-- định nghĩa một đối tượng được nhúng vào tài liệu HTML. Sử dụng thẻ object để nhúng các tệp đa phương tiện( flash, âm thanh, video, PDF .v.v) vào trang của bạn. -->

25. Thẻ output trong HTML
<!-- đại diện cho một kết quả tính toán( như là kết quả cảu một câu lệnh script). -->

26. Thẻ param trong HTML
<!-- định nghĩa các tham số cho các plugin gắn với thẻ object -->

27. Thẻ picture trong HTML
<!-- cho phép các lập trình viên linh hoạt hơn trong qua trình xử lý hình ảnh. -->

28. Thẻ progress trong HTML
<!-- thể hiện quá trình của một tác vụ. -->

29. Thẻ rp trong HTML
<!-- xác định cái gì sẽ hiển thị cho các trình duyệt không hỗ trợ các chú thích ruby -->

30. Thẻ rt trong HTML
<!-- cung cấp thông tin chú thích đó -->

31. Thẻ ruby trong HTML
<!-- chứa một hoặc nhiều ký tự cần chú thích phát âm -->

32. Thẻ source trong HTML
<!-- sử dụng để chỉ định tài nguyên đa phượng tiện cho các phần tử media ví dụ như video, audio, hình ảnh. -->

33. Thẻ sub trong HTML
<!-- văn bản được in thụt xuống dưới so với dòng bình thường -->

34. Thẻ summary trong HTML
<!-- Định nghĩa một tiêu đề của thẻ details. Tiêu đề đc thấy và nội dung bị ẩn đi -->

35. Thẻ sup trong HTML
<!-- văn bản được hiển thị cao hơn so với dòng của văn bản bình thường -->

36. Thẻ time trong HTML
<!-- định nghĩa giá trị thời gian mà con người có thể hiểu được -->
