function getDays(month,year) {
    return new Date(year, month, 0).getDate();
};

function getMonth(selected_month, name){  
    var nameMonth = name.options[name.selectedIndex].text
    console.log(selected_month)
    
    async function getTimeSheet(nameMonth) {
        let apiURL = `http://localhost:3000/${nameMonth}`
    
        let data = await fetch(apiURL).then(res => res.json())
        console.log(data)
        render(data)
    } 
    getTimeSheet(nameMonth)
    
    var current_year = new Date().getFullYear()
    var selected_month = (parseInt(selected_month))
    console.log(selected_month) 
    
    var finTab="";
    var days = getDays(selected_month, current_year)
    function render(data) {
        console.log(data)
        for (var i = 1; i <= days; i++) {
            finTab += `<tr id=signin_${i}>
                            <td>${i}/${selected_month}/${current_year}</td>
                            <td>${data[i].checkIn}</td>
                            <td>${data[i].checkOut}</td>
                        </tr>`
        } 
        console.log(finTab)
        document.getElementById("monthData").innerHTML = finTab;
    }
    
}


